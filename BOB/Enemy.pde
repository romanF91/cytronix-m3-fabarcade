abstract class Enemy extends Creature implements DealsDmg {

  public static final int DROPCHANCE_PERCENT = 30;
  public static final int CORRECT_THIS_LATER = 1;
  
  abstract int getHpGain();
  abstract int getDmgGain();
   
  int cooldown;
  int speed;
  ArrayList<Weapon> weapons;

  int levelFactor;
  int stages;

  public Enemy(int x, int y, int width, int height, int hp, int damage, int speed) {
    super(x, y, width, height, CORRECT_THIS_LATER, CORRECT_THIS_LATER);
    this.setHp(hp + getHpGain() * calculateLevelFactor());
    this.setMaxHp(hp + getHpGain() * calculateLevelFactor());
    this.setDmg(damage + getDmgGain() * calculateLevelFactor());
    this.speed = speed;
    this.weapons = new ArrayList<Weapon>();
  }
  
  // Calculates a Levelfactor to set different Level difficulty
  public int calculateLevelFactor(){
    if (currLvl == null) 
      return 0;
    else
      return currLvl.getNumber() / 5;
      // this is correct and there is no -1 because this always refers to the old level,
      // because calculateLevelFactor is called for every Enemy exactly when the Level constructor is called
      // and not after it has been called
  }

  public void possibleHurt(int dmg) {
    this.hurt(dmg);
  }
  
  /* 
   * overrides the creature attack to add setting the correct booleans
   * this allows enemy-creatures to have the same booleans as players
   * therefore, the same functions can be used by the creature-class,
   * eg getProjPosX(), getProjPosY()
   */
  public void attack(int dir) {
    if (dir == UP) {
      this.attackLeft = false;
      this.attackRight = false;
      this.attackUp = true;
      this.attackDown = false;
    } else if (dir == DOWN) {
      this.attackLeft = false;
      this.attackRight = false;
      this.attackUp = false;
      this.attackDown = true;
    } else if (dir == LEFT) {
      this.attackLeft = true;
      this.attackRight = false;
      this.attackUp = false;
      this.attackDown = false;
    } else if (dir == RIGHT) {
      this.attackLeft = false;
      this.attackRight = true;
      this.attackUp = false;
      this.attackDown = false;
    } else {
      throw new Error("sh*t happened");
    }
    super.attack(dir); 
  }  
  
  public void dropItems(){
    if (percentChance(DROPCHANCE_PERCENT)) generateItem(false, this.x, this.y);
  }
  
  public void reactToObstacleCollision(int dir_from_where_collided){
    // we dont have to do anything here.
    // non-abstract enemys may override this method to handle collisions
    // but just if they want to.
  }
  
  /**
   * Specifies, how enemies move. Also to be considered is the reactToObstacleCollision-Method.
   * \return a direction constant (integer from 0 to 4, where 4 = NOTHING)
   */
  public abstract int movementAI();

  public void die() {
    // possible other stuff, eg drop items
    this.markForDespawn(true);
  }
  
  public int calcSpeed() {
    return this.speed;
  }

  public int getDmg() {
    return this.damage;
  }
  
  public void setDmg(int dmg) {
    this.damage = dmg;
  }
  
  public void showHp(boolean show, int type) {
    if (show && this.hp < this.maxHP) {
      final int DISTANCE = 3;
      final int BAR_BOLDNESS = 3;
      float percent = (float)this.hp / (float) type;
      fill(0, 0, 0);
      // show HP bar on top of enemy if it does not overlap the HUD
      if( (this.getY() - BAR_BOLDNESS - DISTANCE) > HUD_HEIGHT){
        rect(this.getX(), this.getY() - BAR_BOLDNESS - DISTANCE, this.getWidth(), BAR_BOLDNESS);
        fill(200, 0, 0);
        rect(this.getX(), this.getY() - BAR_BOLDNESS - DISTANCE, (int) (percent * this.getWidth()), BAR_BOLDNESS);
      }else{ // show HP bar on bottom of enemy if it would overlap the HUD
        rect(this.getX(), this.getY() + this.getHeight() + DISTANCE, this.getWidth(), BAR_BOLDNESS);
        fill(200, 0, 0);
        rect(this.getX(), this.getY() + this.getHeight() + DISTANCE, (int) (percent * this.getWidth()), BAR_BOLDNESS);
      }
    }
  }
  
  public void updatePosition(){
    this.setDirsToOnly(this.movementAI());
    super.updatePosition();
  } 
  
  public void collision(WObject o) {
    if (o instanceof Projectile && ((Projectile)o).attacksPlayer == false) {
      this.possibleHurt(((DealsDmg)o).getDmg());
    }
    if (o instanceof Obstacle && !this.canFly()) {
      this.reactToObstacleCollision(this.obstacleCollision(o));
    }
  }
  
}

