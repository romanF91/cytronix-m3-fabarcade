class SplitingBombProjectile extends Projectile {
  
  int bombHeight = 8;
  int bombWidth = 8;
  SplitingBombWeapon connectedWeapon;
  
  public SplitingBombProjectile(int x, int y, int direction, boolean b, SplitingBombWeapon w) {
    super(x, y, ITEM_WIDTH, ITEM_HEIGHT, direction, b);  
    this.speed = 1;
    this.attacksPlayer = b;
    this.connectedWeapon = w;
  }

  public void updatePosition(){
    if(distance > range || this.x < 0 || this.x+this.getWidth() > ROOM_WIDTH || this.y < HUD_HEIGHT || this.y+this.getHeight() > ROOM_HEIGHT+HUD_HEIGHT){
       markForDespawn(false); 
    } 
  }  
  
  public SplitingBombWeapon getConnectedWeapon(){
     return this.connectedWeapon; 
  }
  
  public void bombExplode(){
    soundManager.playBombExplosionSound();
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, UP, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, DOWN, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, LEFT, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, RIGHT, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, UPLEFT, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, DOWNLEFT, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, DOWNRIGHT, this.attacksPlayer));
    currRoom.getObjects().add(new BombProjectile(this.x, this.y, UPRIGHT, this.attacksPlayer));
    markForDespawn(true);
    
  }
  public void collision(WObject o){
    //nothing
  }
  public void drawIt() {
    //fill(0,0,0);
    //ellipse(this.x, this.y, bombWidth, bombHeight);
    graphicsManager.showBomb(this.getX(), this.getY(), this.getWidth(), this.getHeight());
  }
}
