class HPItem extends Item {
  public HPItem(int x, int y, int width, int height) {
    super(x, y, width, height);
  }
  
  public void activate() {
    player.hurt(-5);
    if(player.getHp() > player.getMaxHp()){
       player.setHp(player.getMaxHp()); 
    }
  }
  
  public void drawIt() {
    //fill(0, 255, 0);
    //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    
    graphicsManager.showPickupHp(this.getX(), this.getY());
  }
}
