class GraphicsManager{  
   PImage background1;
   PImage backgroundMenu;
   PImage HUDBar;
   PImage Controls;
   PImage boxImage;
   
   PImage enemyBasicShotUp;
   PImage enemyBasicShotDown;
   PImage enemyBasicShotLeft;
   PImage enemyBasicShotRight;
   
   PImage playerBasicShotUp;
   PImage playerBasicShotDown;
   PImage playerBasicShotLeft;
   PImage playerBasicShotRight;
   
   PImage bubbleShot;
   PImage bombProjectile;
   
   PImage bomb;
   
   PImage pickupHp;
   PImage pickupAmmo;
   
   PImage doorOpen;
   PImage doorClosed;
   PImage doorBossOpen;
   PImage doorBossClosed;
   
   PImage hurtEffect;
   
   Animation playerAnimation;
   Animation sideEnemy;
   Animation circlingEnemy;
   Animation flyEnemy;
   Animation antiFly;
   Animation movingBoss;
   Animation movingEnemy;
   Animation bombingBoss;
   Animation splittingBoss;
   Animation splittingBossAdd1;
   Animation splittingBossAdd2;
   Animation splittingBossAdd3;
   Animation acidBoxGreen;
   Animation acidBoxRed;
   Animation acidBoxBlue;
   Animation acidBoxYellow;
   Animation acidBoxViolet;
   
   PImage[] introImages;
  
  
   public GraphicsManager(){
      //--
   } 
    
   public void loadGraphics(){
       
       
       background1    = loadImage("image/background/background3.png");
       backgroundMenu = loadImage("image/background/background_space.png");
       boxImage       = loadImage("image/obstacles/acid_box0.png");
       HUDBar         = loadImage("image/HUDBar.png");
       Controls       = loadImage("image/controls.png");
       
       enemyBasicShotUp     = loadImage("image/projectiles/enemyShotUp.png");
       enemyBasicShotDown   = loadImage("image/projectiles/enemyShotDown.png");
       enemyBasicShotLeft   = loadImage("image/projectiles/enemyShotLeft.png");
       enemyBasicShotRight  = loadImage("image/projectiles/enemyShotRight.png");
       
       playerBasicShotUp    = loadImage("image/projectiles/playerShotUp.png");
       playerBasicShotDown  = loadImage("image/projectiles/playerShotDown.png");
       playerBasicShotLeft  = loadImage("image/projectiles/playerShotLeft.png");
       playerBasicShotRight = loadImage("image/projectiles/playerShotRight.png");
       
       bubbleShot = loadImage("image/projectiles/bubble.png"); 
       bombProjectile = loadImage("image/projectiles/bombProjectile.png");
       
       bomb = loadImage("image/projectiles/bomb.png");
       
       pickupHp   = loadImage("image/pickupHp.png");
       pickupAmmo = loadImage("image/pickupAmmo.png");
       
       doorOpen       = loadImage("image/obstacles/doorOpen.png");
       doorClosed     = loadImage("image/obstacles/doorClosed.png");
       doorBossOpen   = loadImage("image/obstacles/doorBossOpen.png");
       doorBossClosed = loadImage("image/obstacles/doorBossClosed.png");
       
       // load & create animations
       playerAnimation = new Animation("image/player/player",8, 12);
       circlingEnemy   = new Animation("image/circlingEnemy/buzzSaw",2, 8);
       sideEnemy       = new Animation("image/sideEnemy/enemy",2, 16);
       flyEnemy        = new Animation("image/fly/fly",2, 8);
       movingBoss      = new Animation("image/movingBoss/robot", 8, 32);
       movingEnemy     = new Animation("image/MovingEnemy/slime", 8, 16);
       bombingBoss     = new Animation("image/bombBoss/bombBoss", 2,32);
       splittingBoss      = new Animation("image/SplittingBoss/slime", 8, 64);
       splittingBossAdd1  = new Animation("image/SplittingBossAdd1/slime", 8, 32);
       splittingBossAdd2  = new Animation("image/SplittingBossAdd2/slime", 8, 16);
       splittingBossAdd3  = new Animation("image/SplittingBossAdd3/slime", 8, 8);
       antiFly         = new Animation("image/antiFly/spider", 2, 16);
       acidBoxGreen    = new Animation("image/obstacles/acid_box",2,20);
       acidBoxRed      = new Animation("image/obstacles/acid_box_red",2,20);
       acidBoxBlue     = new Animation("image/obstacles/acid_box_blue",2,20);
       acidBoxYellow   = new Animation("image/obstacles/acid_box_yellow",2,20);
       acidBoxViolet   = new Animation("image/obstacles/acid_box_violet",2,20);
       
       //load intro images
       introImages = new PImage[8];
       for (int i = 0; i < 8; i++) {
        // Use nf() to number format 'i' into string digits
        String filename = "image/intro/intro" + nf(i,1) + ".png";
        introImages[i] = loadImage(filename);
       }
       
       hurtEffect = loadImage("image/HurtEffect.png");
   }
   
   public PImage[] getIntroImages(){
      return this.introImages; 
   }
   
   public int showPlayerImage(int x, int y, int width, int height, int dir, int attackDir, int counter){
       //image(playerImage, x, y, width, height);
     return playerAnimation.display(x,y,player.calcSpeed(), dir, attackDir, counter); 
   }
   
   public void showRoomBackground(int type){
       if(type == 1) image(background1, 0, HUD_HEIGHT); 
   }
   
   public void showMenuBackground(){
      image(backgroundMenu, 0,0); 
   }
   
   public void showHUDBar(){
      image(HUDBar, 0,0, WINDOW_WIDTH, HUD_HEIGHT); 
   }
   
   public void showControls(){
      image(Controls,0,0); 
   }
   
   public void showHurtImage(int trans) {
     tint(0, trans);
     image(hurtEffect, 0, HUD_HEIGHT, WINDOW_WIDTH, ROOM_HEIGHT);
     noTint(); 
   }
   
   public void showObstacleImage(int x, int y, int width, int height, int type){
       // show wooden box
       if(type == 1) image(boxImage, x, y); 
   }
   
   public void showEnemyBasicShot(int x, int y, int width, int height, int dir){
       if(dir == UP)    image(enemyBasicShotUp, x, y);
       if(dir == DOWN)  image(enemyBasicShotDown, x, y);
       if(dir == LEFT)  image(enemyBasicShotLeft, x, y);
       if(dir == RIGHT) image(enemyBasicShotRight, x, y);
   }
   
   public void showPlayerBasicShot(int x, int y, int width, int height, int dir){
       if(dir == UP)    image(playerBasicShotUp, x, y);
       if(dir == DOWN)  image(playerBasicShotDown, x, y);
       if(dir == LEFT)  image(playerBasicShotLeft, x, y);
       if(dir == RIGHT) image(playerBasicShotRight, x, y);
   }
   
   public void showBubbleShot(int x, int y, int width, int height){
      image(bubbleShot, x, y); 
   }
   
   public void showBomb(int x, int y, int width, int height){
      image(bomb, x, y); 
   }
   
   public void showBombProjectile(int x, int y, int width, int height){
      image(bombProjectile, x, y); 
   }
   
   public void showPickupHp(int x, int y){
      image(pickupHp, x, y); 
   }
   
   public void showPickupAmmo(int x, int y){
      image(pickupAmmo, x, y); 
   }
   
   public void showDoorOpenImage(int x, int y, boolean type){
     if(type)  image(doorBossOpen, x, y); 
     if(!type) image(doorOpen, x, y); 
   }
   
   public void showDoorCloseImage(int x, int y, boolean type){
      if(type)  image(doorBossClosed, x, y);
      if(!type) image(doorClosed, x, y);
   }
   
   public int showSideEnemyImage(int x, int y, int speed, int dir, int counter){
      return sideEnemy.display(x,y,speed,dir, NOTHING, counter);
   }
   
   public int showBoxAnimation(int x, int y, int speed, int dir, int counter, int level){
      if(level % 5 == 1)
      return acidBoxGreen.display(x,y,speed,dir, NOTHING, counter);
      else if(level % 5 == 2)
      return acidBoxRed.display(x,y,speed,dir,NOTHING,counter);
      else if(level % 5 == 3)
      return acidBoxBlue.display(x,y,speed,dir,NOTHING,counter);
      else if(level % 5 == 4)
      return acidBoxYellow.display(x,y,speed,dir,NOTHING,counter);
      else
      return acidBoxViolet.display(x,y,speed,dir,NOTHING,counter);
      
   }
   
   public int showCirclingEnemyImage(int x, int y, int speed, int dir, int counter){
      return circlingEnemy.display(x, y, speed, dir, NOTHING, counter);
   }
   
   public int showFlyEnemyImage(int x, int y, int speed, int dir, int counter){
      return flyEnemy.display(x, y, speed, dir, NOTHING, counter);
   }
   
   public int showMovingBossImage(int x, int y, int speed, int dir, int counter){
      return movingBoss.display(x, y, speed, dir, NOTHING, counter);
   }
   public int showMovingEnemyImage(int x, int y, int speed, int dir, int counter){
      return movingEnemy.display(x, y, speed, dir, NOTHING, counter);
   }
   public int showAntiFlyImage(int x, int y, int speed, int dir, int counter){
      return antiFly.display(x, y, speed, dir, NOTHING, counter); 
   }
   public int showSplittingBossImage(int x, int y, int speed, int dir, int counter){
      return splittingBoss.display(x, y, speed, dir, NOTHING, counter);
   }
   public int showSplittingBossAdd1Image(int x, int y, int speed, int dir, int counter){
      return splittingBossAdd1.display(x, y, speed, dir, NOTHING, counter);
   }
   public int showSplittingBossAdd2Image(int x, int y, int speed, int dir, int counter){
      return splittingBossAdd2.display(x, y, speed, dir, NOTHING, counter);
   }
   public int showSplittingBossAdd3Image(int x, int y, int speed, int dir, int counter){
      return splittingBossAdd3.display(x, y, speed, dir, NOTHING, counter);
   }
   public int showBombingBossImage(int x, int y, int speed, int dir, int counter){
      return bombingBoss.display(x,y,speed,dir,NOTHING,counter); 
   }
}
