class BubbleProjectile extends Projectile {
  int variation = 0;
  
  public BubbleProjectile(int x, int y, int direction, boolean attacksPlayer) {
    super(x, y, ITEM_WIDTH-1, ITEM_HEIGHT-1, direction, attacksPlayer);
    this.speed  = 1;
    this.damage = 1;
    this.range  = 300;
  }
  
  public void updatePosition(){
    if(this.direction == UP) this.y-= speed;  //move up
    if(this.direction == LEFT) this.x-= speed;  //move left
    if(this.direction == DOWN) this.y+= speed;  //move down
    if(this.direction == RIGHT) this.x+= speed;  //move right
    
    
    // ==============================================================
    // BLASENFEELING 
    // ==============================================================
    // Chance für Blasenfeeling-Umstellung in eine andere Richtung
    if(percentChance(25)) {
      if (this.variation == -1 || this.variation == +1) {
        this.variation = 0;
      } else { // it's 0
        this.variation = (percentChance(50) ? -1 : 1) ; 
      }
    }
    // Blasenfeeling ausführen!
    if(percentChance(60)) {
      if(this.direction == UP || this.direction == DOWN)
        this.x += variation;
      else // LEFT or RIGHT
        this.y += variation;
    }
    // ==============================================================
    
    distance+=speed;
    if(distance > range || this.x < 0 || this.x+this.getWidth() > ROOM_WIDTH || this.y < HUD_HEIGHT || this.y+this.getHeight() > ROOM_HEIGHT+HUD_HEIGHT){
       markForDespawn(false); 
    } 
  }  
  
  public void drawIt() {
    //fill(0,191,230);
    //ellipse(this.x, this.y, ITEM_WIDTH, ITEM_HEIGHT);
    graphicsManager.showBubbleShot(this.x, this.y, ITEM_WIDTH, ITEM_HEIGHT);
  }
}
