/*
 *  This function displays the HUD
 */
public void drawHud() {
  
  textFont(hud);
  Room [][] mapRooms = currLvl.getRooms();

  textAlign(CORNER);
  textSize(8);
  fill(255,215,70);
  // text("ObjectListSize: " + currRoom.getObjects().size(), 10, 20);  // shows number of objects in the shown room (debugging)
  
  // show health points & weapon charges & weapon name
  if (player != null) {
    
    //show player hp
    float percent = (float) player.getHp() / (float) player.getMaxHp();
    fill(0);
    rect(WINDOW_WIDTH*0.75 +15, 9, 60, 6);
    // slide from fill(255,215,70) to (255,28,28) when having less life
    fill(230+25*percent,30+185*percent,30+40*percent);
    rect(WINDOW_WIDTH*0.75 +15, 9, (int) (percent * 60), 6);
    fill(255,215,70);
    text("HP: ", WINDOW_WIDTH*0.75-10, 15);
    
    if(player.getCurrWeapon().getName() != "BASIC ATTACK") text("AMMO: " + player.getCurrWeapon().getCharges(), WINDOW_WIDTH*0.75-10, 30);
    else text("AMMO: --" , WINDOW_WIDTH*0.75-10, 30);
    
    // show weapon name and level
    textAlign(CENTER);
    text("WEAPON:", WINDOW_WIDTH*0.5+5, 15);
    if (player.getCurrWeapon().getLevel() > 1) text(player.getCurrWeapon().getName() + " " + player.getCurrWeapon().getLevel(), WINDOW_WIDTH*0.5+5, 30);
    else text(player.getCurrWeapon().getName(), WINDOW_WIDTH*0.5+5, 30);
    
    // show number of current level
    textAlign(RIGHT);
    text("LEVEL", WINDOW_WIDTH/3 -48, 15);
    text(currLvl.getNumber(), WINDOW_WIDTH/3-33, 30);
  }
  else {
    text("HP: 0", WINDOW_WIDTH*0.75-10, 20);
  }
  
  // draws minimap
  for(int i = 0; i<currLvl.MAP_HEIGHT; i++){
     for(int j = 0; j < currLvl.MAP_WIDTH; j++){
        if(mapRooms[i][j] != null && mapRooms[i][j].isCompleted() || mapRooms[i][j] == currRoom){
           if(mapRooms[i][j].isCurrRoom()) {
             fill(255,28,28);
           }else{
             fill(255,215,70);
           }
           // if(mapRooms[i][j].isBossRoom()) fill(0,0,255); //shows boss room for testing
           rect(8*j+5, 6*i+5, 7, 5 );
           if(i-1 >= 0){
              if((mapRooms[i-1][j] != null) && !mapRooms[i-1][j].isCompleted() && (mapRooms[i-1][j] != currRoom)){
                 fill(110,90,0);
                 rect(8*j+5, 6*(i-1)+5, 7,5); 
              }
           }
           if(i+1 < currLvl.MAP_HEIGHT){
              if((mapRooms[i+1][j] != null) && !mapRooms[i+1][j].isCompleted() && (mapRooms[i+1][j] != currRoom)){
                 fill(110,90,0);
                 rect(8*j+5, 6*(i+1)+5, 7,5); 
              }
           }
           if(j-1 >= 0){
              if((mapRooms[i][j-1] != null) && !mapRooms[i][j-1].isCompleted() && (mapRooms[i][j-1] != currRoom)){
                 fill(110,90,0);
                 rect(8*(j-1)+5, 6*i+5, 7,5); 
              }
           }
           if(j+1 < currLvl.MAP_WIDTH){
              if((mapRooms[i][j+1] != null) && !mapRooms[i][j+1].isCompleted() && (mapRooms[i][j+1] != currRoom)){
                 fill(110,90,0);
                 rect(8*(j+1)+5, 6*i+5, 7,5); 
              }
           }
        }
     } 
  }
  
  //fill(200, 200, 200);
  fill(0);
  noStroke();
  graphicsManager.showHUDBar();
}




/*
 * This function detects collision between two WObjects.
 * \param o1 the first Wobject
 * \param o2 the second Wobject
 * \return a bool indicating whether or not those object collide
 */
public boolean collision(WObject o1, WObject o2) {

  if ((o1.getX()+o1.getWidth()) < o2.getX() || o1.getX() > (o2.getX()+o2.getWidth())) return false;
  if ((o1.getY()+o1.getHeight()) < o2.getY() || o1.getY() > (o2.getY()+o2.getHeight())) return false;
  return true;
}


public boolean percentChance (int percent) {
  return ((int) random(1,101) <= percent);
}

public static final int NUMBER_OF_BOSS_ITEMS=3;
public static final int NUMBER_OF_NORMAL_ITEMS=2;

public void generateItem(boolean bossDrop, int x, int y) {
  if (bossDrop) {
    int rnd = ((int) random(0,NUMBER_OF_BOSS_ITEMS));
    if (rnd == 0) this.currRoom.getObjects().add(new MultipleShotWeapon(x, y, ITEM_WIDTH, ITEM_HEIGHT, 1, false));
    if (rnd == 1) this.currRoom.getObjects().add(new BubbleWeapon(x, y, ITEM_WIDTH, ITEM_HEIGHT, 1, false));
    if (rnd == 2) this.currRoom.getObjects().add(new SplitingBombWeapon(x, y, ITEM_WIDTH, ITEM_HEIGHT, 1, false));
    this.currRoom.getObjects().add(new LvlDoor(ROOM_WIDTH/2-8, ROOM_HEIGHT/2+HUD_HEIGHT-8));
  } else {
    int rnd = ((int) random(0,NUMBER_OF_NORMAL_ITEMS));
    if (rnd == 0) this.currRoom.getObjects().add(new HPItem(x, y, ITEM_WIDTH, ITEM_HEIGHT));
    if (rnd == 1) this.currRoom.getObjects().add(new RechargeItem(x, y, ITEM_WIDTH, ITEM_HEIGHT));
  }
}


public static final int     WINDOW_WIDTH = 320;
public static final int     WINDOW_HEIGHT = 240;
public static final int     ROOM_WIDTH = WINDOW_WIDTH;
public static final int     ROOM_HEIGHT = 202;
public static final int     HUD_HEIGHT = WINDOW_HEIGHT - ROOM_HEIGHT;
public static final int     PLAYER_BASIC_SPEED = 2;
public static final int     STANDARD_ENEMY_HP = 5;
public static final int     BOSS_ENEMY_HP = 20; // 20 is like playable (just for atm)
public static final int     PLAYER_HP = 20;
public static final boolean SHOW_STANDARD_ENEMY_HP = true;
public static final boolean SHOW_BOSS_ENEMY_HP = true;
public static final int     ITEM_WIDTH = 6;
public static final int     ITEM_HEIGHT = 6;
public              int     GAME_STATE = 0;
public              boolean MENU_START = false;
public              int     MENU_CHOICE = 0;   
public              boolean NEW_GAME = false;
public              boolean FIRST_START = true;
public              boolean MENU_CHANGE = true;
public static final int     COLOR_DELAY = 550;
public              int     START_TIME  = 0;
public              int     START_TIME2 = 0;
public              boolean CHEAT = false;
public              boolean BACKGROUND_SHOWN = false;
public              Effect  currEffect = null;
public              Door    currDoor = null; // recently entered Door... roomchange will be executed if != null


// DIRECTION CONSTANTS
public static final int RIGHT = 0, LEFT = 1, UP = 2, DOWN = 3, NOTHING = 4, UPLEFT=5, DOWNLEFT=6, UPRIGHT=7, DOWNRIGHT=8;
// GAME_STATE CONSTANTS
public static final int RUNNING = 0, PAUSE = 1, GAME_OVER = 3, MAIN_MENU = 4, CONTROLS = 5, SHOW_HIGHSCORE = 6, INSERT_HIGHSCORE = 7;
public int LAST_GAME_STATE = NOTHING;

PFont hud;
PFont logo;
PFont credits;
Room currRoom;
Level currLvl;
Player player;
SoundManager soundManager;
GraphicsManager graphicsManager;
Intro intro;
Highscore highscore;


// Highscore Variablen
int[] name = new int[8];
int choice;
char[] alphabet = {' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};


void setup() {
  size(WINDOW_WIDTH, WINDOW_HEIGHT);
  noStroke();
  fill(255);
  rectMode(CORNER);
  ellipseMode(CORNER);
  // load fonts
  hud  = loadFont("hud.vlw");  //sets hud font
  logo = loadFont("logo.vlw"); //sets logo font
  
  // initialize sound & music
  soundManager = new SoundManager();
  
  //Loading highscore
  highscore = new Highscore("highscore.txt");
  
  // initialize graphics
  graphicsManager = new GraphicsManager();
  graphicsManager.loadGraphics();
  
  //sets game state 
  GAME_STATE = MAIN_MENU;  
  NEW_GAME   = true; 
 
  //load intro
  intro = new Intro(); 
 
}

void setupGame(){
  //create new Lvl
  currLvl = new Level(new Room(false), 5, 1, 1);
  currLvl.addRooms();
  currRoom  = currLvl.getStartRoom();
  currRoom.setCurrRoom(true);
  frameRate(40);
  // spawn-functions to create enemys and player
  currRoom.spawn("Player", 1 );
  //currRoom.spawn("test_item", 2);
}

void runGame(){
  
  if(NEW_GAME){
    setupGame(); 
    NEW_GAME = false;
    soundManager.playIngameMusic();
  }
  
  if(currRoom.isBossRoom()){
    soundManager.stopIngameMusic();
    soundManager.playBossMusic();
  }else{
    soundManager.stopBossMusic();
    soundManager.playIngameMusic();
  }
    
  player = currRoom.getPlayer();
    
  // possibly change currently displayed Room
  if (currDoor != null) {
    if (!(currDoor instanceof LvlDoor))                  // normal door
      currLvl.loadNextRoom((currDoor).getPosition());
    else                                                 // LvlDoor
      ((LvlDoor)currDoor).loadNextLvl();
    currDoor = null;

  // otherwise, game takes place in the current Room
  } else {
    currentRoomProgressAndDraw();
  }
   
  drawHud();
    
  if(MENU_START){
    MENU_START = false;
    GAME_STATE = PAUSE;
  }
}

void currentRoomProgressAndDraw() {

  background(0);
  graphicsManager.showRoomBackground(1);
  ArrayList<WObject> objects = currRoom.getObjects();
  // list has to be copied to create new attack-objects without 
  // errors for every Object which has a an attack-method
  ArrayList<WObject> copyList = new ArrayList<WObject>(objects);
  
  //updates object positions & checks if enemy uses attack
  for (WObject object : copyList) {
    if (object instanceof Movable) {
      ((Movable) object).updatePosition();
    }
    if (object instanceof Creature)
      ((Creature)object).possibleAttack();
  }


  // collision detection
  for (WObject o1 : objects) {
    if (CHEAT && (o1 instanceof Enemy)) ((Enemy)o1).die();
    if (o1 instanceof Movable) {
      for (WObject o2 : objects) {
        if ((o1 instanceof Projectile && o2 instanceof Projectile) ||(o1 == o2)) continue; // efficiency. there are many of these on the map.
        if (collision(o1, o2)){
          ((Movable) o1).collision(o2);
        }
      }
    }
  } 

  // checks for every room if all enemies are killed and unlocks the doors
  currRoom.checkCompletion();
  
  if(currRoom.isCompleted()){
    currRoom.unlockDoors();
  }else{
    currRoom.lockDoors();
  }

  // despawn objects
  int obj_deleted = 0;
  for (int i=0; i<objects.size()-obj_deleted; i++) {
    WObject o = objects.get(i);
    if (o.despawn) {
      o.despawn();
      i--;
      obj_deleted++;
      // o.despawnWay = ? handle it later.
    }
  }

  // DRAW! (first obstacles then creatures)
  for (WObject object : objects){
     if(!(object instanceof Creature)) object.drawIt(); 
  }
  for (WObject object : objects){
     if(object instanceof Creature) object.drawIt(); 
  }
}

void displayControls(){
   LAST_GAME_STATE = GAME_STATE;
   GAME_STATE = CONTROLS;
}

void draw() {
  soundManager.update();
  //******* PAUSE MENU *********
  if(GAME_STATE == PAUSE){
      
      textAlign(CENTER);
      if(!BACKGROUND_SHOWN){
        BACKGROUND_SHOWN = true;
        fill(0,0,0,150);
        rect(0,0, ROOM_WIDTH, ROOM_HEIGHT+HUD_HEIGHT);
      }
      fill(255); 
     
      if(MENU_CHOICE < 0)   MENU_CHOICE = 2;
      if(MENU_CHOICE > 2)   MENU_CHOICE = 0;
     
      // menu options
      textFont(hud);
      textSize(10);
      if(MENU_CHOICE == 0) fill(0, 255, 255);
      else fill(255);
      text("RESUME", WINDOW_WIDTH/2, WINDOW_HEIGHT/2);
      
      if(MENU_CHOICE == 1) fill(0, 255, 255);
      else fill(255);
      text("CONTROLS", WINDOW_WIDTH/2, WINDOW_HEIGHT/2 + 25);
      
      if(MENU_CHOICE == 2) fill(0, 255, 255);
      else fill(255);
      text("EXIT TO MAIN MENU", WINDOW_WIDTH/2, WINDOW_HEIGHT/2 + 50);
     
     
     if(MENU_START){
        if(MENU_CHOICE == 0){
           GAME_STATE = RUNNING; 
        }
        if(MENU_CHOICE == 1){
           displayControls(); 
        }
        if(MENU_CHOICE == 2){
           MENU_START = false; 
           NEW_GAME   = true;
           GAME_STATE = MAIN_MENU;    
        }
        BACKGROUND_SHOWN = false;
        MENU_START = false; 
     }
  }
  
  //******* CONTROLS ***********
  if(GAME_STATE == CONTROLS){
     background(0);
     graphicsManager.showControls();
     textAlign(CORNER);
     textFont(hud);
     fill(200);
     
     textSize(8);
     text("START", 73, 40);
     text("MOVE", 50, 210);
     text("ATTACK", 162, 33);
     text("SWITCH WEAPON", 210, 60);
     text("ACTIVATE BOMBS", 190, 215);
     
     
     if(MENU_START){
       if(LAST_GAME_STATE == PAUSE) LAST_GAME_STATE = RUNNING;
       GAME_STATE = LAST_GAME_STATE;
       MENU_START = false;
     } 
  }
  
  //******* SHOW HIGHSCORE ***********
  if(GAME_STATE == SHOW_HIGHSCORE){
     background(0);
     textAlign(CORNER);
     textFont(hud);
     textSize(20);
     text("Highscore:",60,45);
     textSize(10);
     fill(255);
     for(int i = 0; i< highscore.getEntryCount(); i++) {
       text((i+1) + "." , 40, 70+i*15);
       text(highscore.getEntryName(i) , 90, 70+i*15);
       text(highscore.getEntryLevel(i) , 220, 70+i*15);
     }
     
     
     
     if(MENU_START){
       GAME_STATE = MAIN_MENU;
       MENU_START = false;
     } 
  }
  
  //******* MAIN MENU **********
  if(GAME_STATE == MAIN_MENU){
    soundManager.stopMusic();
    
   if(NEW_GAME){
      if(intro.FINISHED) soundManager.playMenuMusic();
      NEW_GAME = false;
   }
   
   if(intro.FINISHED){
    background(0);
    graphicsManager.showMenuBackground();
    textAlign(CENTER);
    fill(0);
    
    //handles menu choice options
    if(MENU_CHOICE < 0) MENU_CHOICE = 3;
    if(MENU_CHOICE > 3) MENU_CHOICE = 0;

    
    if(START_TIME + COLOR_DELAY < millis()){
      START_TIME = millis();
      stroke((int)random(100,256),(int)random(100,256),(int)random(100,256));
    }
    
    /*
    // frame around the logo
    line(35,15,WINDOW_WIDTH-35, 15);
    line(WINDOW_WIDTH-35, 15, WINDOW_WIDTH-15, 225);
    line(WINDOW_WIDTH-15, 225, 15,225);
    line(15,225,35,15);
    
    line(40,20,WINDOW_WIDTH-40, 20);
    line(WINDOW_WIDTH-40, 20, WINDOW_WIDTH-20, 220);
    line(WINDOW_WIDTH-20, 220, 20,220);
    line(20,220,40,20);
    //quad(35, 15, WINDOW_WIDTH-35, 15, WINDOW_WIDTH-15, 225, 15, 225);
    //quad(40, 20, WINDOW_WIDTH-40, 20, WINDOW_WIDTH-20, 220, 20, 220);
    */
    

    // logo
    fill(16,255,0);
    textFont(logo);
    textSize(32);
    text("CYTRONIX", WINDOW_WIDTH/2, WINDOW_HEIGHT*0.25);
    
    // copyright/credits
    fill(255);
    textFont(hud);
    textSize(8);
    text("- 2013 M3 RWTH AACHEN -", WINDOW_WIDTH/2, WINDOW_HEIGHT*0.75+40);
   }
    if(FIRST_START || !intro.FINISHED){
      
      if(!intro.FINISHED){
       intro.play(); 
      }else{
      
      textFont(hud);
      textSize(10);

      text("INSERT COIN TO START",WINDOW_WIDTH/2, WINDOW_HEIGHT*0.6);
      }
    }else if(!FIRST_START && intro.FINISHED){

      // menu options
      textFont(hud);
      textSize(10);
      if(MENU_CHOICE == 0) fill(0, 255, 255);
      else fill(255);
      text("START GAME", WINDOW_WIDTH/2, WINDOW_HEIGHT/2 -5);
      
      if(MENU_CHOICE == 1) fill(0,255,255);
      else fill(255);
      text("HIGHSCORE", WINDOW_WIDTH/2, WINDOW_HEIGHT/2 + 15);
      
      if(MENU_CHOICE == 2) fill(0, 255, 255);
      else fill(255);
      text("CONTROLS", WINDOW_WIDTH/2, WINDOW_HEIGHT/2+35);
      
      
      if(MENU_CHOICE == 3) fill(0, 255, 255);
      else fill(255);
      text("EXIT", WINDOW_WIDTH/2, WINDOW_HEIGHT/2 + 55);
      
      if(MENU_START){
         MENU_START = false;
         if(MENU_CHOICE == 0) {
           NEW_GAME = true;
           soundManager.stopMenuMusic();
           GAME_STATE = RUNNING;
         }
         if(MENU_CHOICE == 1) {
           GAME_STATE = SHOW_HIGHSCORE;
         } 
         if(MENU_CHOICE == 2) displayControls();
         if(MENU_CHOICE == 3) exitGame();   
      }
    }
  }
  
  if(GAME_STATE == GAME_OVER){
     soundManager.stopMusic();
     background(0);
     textFont(hud);
     textAlign(CENTER);
     textSize(14);
     fill(200,0,0);
     text("GAME OVER!", WINDOW_WIDTH/2, WINDOW_HEIGHT/2-20);
     
     fill(255); 
     textSize(10);
     text("PRESS START", WINDOW_WIDTH/2, WINDOW_HEIGHT/2+50);
     
     if(MENU_START){
       if(highscore.inHighscore(currLvl.getNumber())) {
         choice = 0;
         GAME_STATE = INSERT_HIGHSCORE;
       }
       else {
         GAME_STATE = MAIN_MENU;
         MENU_START = false; 
         NEW_GAME   = true;
       }
     }
  }
  
  
  //******* INSERT HIGHSCORE **********
  if(GAME_STATE == INSERT_HIGHSCORE){
    
    background(0);
    textAlign(CORNER);
    textFont(hud);
    textSize(20);
    fill(200,0,0);
    textAlign(CENTER);
    text("INSERT NAME:", WINDOW_WIDTH/2,40);
    textAlign(CORNER);

    textSize(16);
    for(int i = 0; i< name.length; i++) {
      if(choice==i) fill(200,200,0);
      else fill(200,0,0);
      text (alphabet[name[i]],55+i*20,80);
      rect(53+i*20, 83, 18, 2);
    }
    if(choice==8) fill(200,200,0);
    else fill(200,0,0);
    text ("OK",220,80);
    
    if(MENU_START && choice == 8) {
      String n = "";
      for(int i = 0; i< name.length; i++) {
        n+=alphabet[name[i]];
      }
      highscore.insert(n,currLvl.getNumber());
      GAME_STATE = MAIN_MENU;
      MENU_START = false; 
      NEW_GAME   = true;
    }
    
  }
  
  
  //******* GAME RUNS **********
  if(GAME_STATE == RUNNING){
    checkEffect();
    if(!effectStop()) runGame();
    drawEffect();
  }
}

void checkEffect(){
  if (currEffect != null && currEffect.over) currEffect = null;
}

void drawEffect() {
  if (currEffect != null) currEffect.drawIt();
}

boolean effectStop() {
  boolean stop = false;
  if (currEffect != null) stop = currEffect.stopgame;
  return stop;
} 

void keyPressed() {
  if(player != null){
    if (key == 'w') player.moveUp=true;
    if (key == 'd') player.moveRight=true;
    if (key == 's') player.moveDown=true;      
    if (key == 'a') player.moveLeft=true;  
    if (key == 'i') player.attackUp=true;
    if (key == 'j') player.attackLeft=true;
    if (key == 'k') player.attackDown=true;     
    if (key == 'l') player.attackRight=true;
    if (key == 't') player.switchWeapon(1);
    if (key == 'u') player.switchWeapon(-1);
    if (key == 'n') {
      Weapon p = player.getCurrWeapon();
      p.activate();
    }
  }
  if (key == 'w') {
    if(MENU_CHANGE) MENU_CHOICE--;
    MENU_CHANGE = false;
    
  }
  if (key == 's') {
     if(MENU_CHANGE) MENU_CHOICE++;
     MENU_CHANGE = false; 
  }
  if (key == '1') {
      if(intro.FINISHED) MENU_START = true;
      intro.setFinished(true);
      soundManager.playPickupSound(); 
  }
  if (key == 'x') CHEAT=true;
  if (key == 'c') {
      FIRST_START = false;
      soundManager.playPickupSound();   
  }
  
  if (key == 'd' && GAME_STATE == INSERT_HIGHSCORE) choice = ((choice<8) ? choice+1 : 8);    
  if (key == 'a' && GAME_STATE == INSERT_HIGHSCORE) choice = ((choice>0) ? choice-1 : 0); 
  if (key == 'w' && GAME_STATE == INSERT_HIGHSCORE) {
    if(choice<8) name[choice] = ((name[choice] < alphabet.length-1) ? name[choice]+1 : alphabet.length -1);  
  }  
  if (key == 's' && GAME_STATE == INSERT_HIGHSCORE) {
    if(choice<8) name[choice] = ((name[choice] > 0) ? name[choice]-1 : 0);  
  }
}

void keyReleased() {
  if(player != null){
    if (key == 'w') player.moveUp=false;
    if (key == 'd') player.moveRight=false;
    if (key == 's') player.moveDown=false;      
    if (key == 'a') player.moveLeft=false;  
    if (key == 'i') player.attackUp=false;
    if (key == 'j') player.attackLeft=false;
    if (key == 'k') player.attackDown=false;     
    if (key == 'l') player.attackRight=false;
  }
  if (key == 'w') MENU_CHANGE = true;
  if (key == 's') MENU_CHANGE = true;
  if (key == '1') {
    MENU_START = false;
    MENU_CHOICE = 0;
  }
  if (key == 'x') CHEAT=false;
}

//Clean up everything
void exitGame() {
  soundManager.stopMenuMusic();
  soundManager.stopIngameMusic();
  exit(); 
}
