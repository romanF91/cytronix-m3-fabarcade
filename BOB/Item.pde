abstract class Item extends StaticObject {
  
  public static final int WIDTH=8;
  public static final int HEIGHT=8;
  
  public Item(int x, int y, int width, int height) {
    super(x, y, width, height);
  }
  
  public abstract void activate();
  
}
