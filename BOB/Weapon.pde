abstract class Weapon extends Item {
  
  private int usesLeft;
  private String name;
  private int level;
  public static final int MAX_LEVEL = 5;
  boolean attacksPlayer;
  
  protected int INIT_CHARGES;
  protected int INIT_COOLDOWN;
  protected int CD_BUFF_PER_LEVEL;
  protected int CHARGES_BUFF_PER_LEVEL;
  
  public Weapon(int x, int y, int width, int height, int level, boolean b) {
    super(x, y, width, height);
    this.level = level;
    this.attacksPlayer = b;
  }
  
  /*
   * Levels up the weapon.
   * \return boolean successful or not
   */
  public boolean levelUp() {
    if (this.level < this.MAX_LEVEL) {
      this.level++;
      return true;
    } else {
      return false;
    }
  }
  public boolean getAttacksPlayer(){
    return this.attacksPlayer; 
  }
  
  public int getLevel() {
    return this.level;
  }
  
  public void recharge() {
    this.usesLeft = getInitCharges();
  }
  
  public void setName(String s){
      this.name = s;
  }
  public String getName(){
     return this.name; 
  }
  public int getUsesLeft(){
     return this.usesLeft; 
  }
  
  private int getInitCharges(){
    return this.INIT_CHARGES + this.CHARGES_BUFF_PER_LEVEL * (this.getLevel()-1);
  }
  
  public int getCooldown(){
    return this.INIT_COOLDOWN - this.CD_BUFF_PER_LEVEL * (this.getLevel()-1);
  }
  
  protected abstract void spawnProjectiles(int startX, int startY, int dir);
  
  public void shoot(int startX, int startY, int dir) {
    if (this.usesLeft > 0) {
      this.spawnProjectiles(startX, startY, dir);
      this.usesLeft--; 
    } else {
      this.noUsesLeft();
      if (!player.getCurrWeapon().getName().equals("BOMB")) player.setCurrWeaponPos(0);
    }
  }
  
  public int getCharges(){
     return usesLeft; 
  }
  
  public void noUsesLeft() {
    soundManager.playOutOfAmmoSound();
  }
}
