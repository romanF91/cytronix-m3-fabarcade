class BombingFlyBoss extends BossEnemy{
  
  public static final int BOMBING_FLYBOSS_HP = BOSS_ENEMY_HP;
  public static final int BOMBING_FLYBOSS_SPEED = 1;
  public static final int BOMBING_FLYBOSS_DMG = 5;
  
  public int getDmgGain() { return 3; }
  public int getHpGain()  { return BOSS_ENEMY_HP / 2; }

  int dir = 2;
  int attackDir = 0;

  private int startTime = 0; 
  private int attackCD = 1000;  // delay till next bombactivation
  Weapon w;

  public BombingFlyBoss(int x, int y){
    super(x, y, 32, 32, BOMBING_FLYBOSS_HP, BOMBING_FLYBOSS_DMG, BOMBING_FLYBOSS_SPEED);
    if(currLvl != null) this. setMaxHp(BOMBING_FLYBOSS_HP+(calculateLevelFactor()*5));
    this.cooldown = 1000;  // delay till next bomb
    this.speed = 2;
    this.getWeapons().add(new SplitingBombWeapon(0, 0, ITEM_WIDTH, ITEM_HEIGHT, 0,true));  
    w = this.getCurrWeapon();  
  } 

  public int getAttackDir() {
    if(startTime + attackCD < millis()){
      soundManager.playHurtSound();
      startTime = millis();      
      w.activate();
    }
    return UP;
  }
  
  public int movementAI(){
     if (player == null) return NOTHING;
    int dir1 = NOTHING;
    int dir2 = NOTHING;
    if(this.x >= player.getX()) dir1 = LEFT;
    if(this.x < player.getX()) dir1 = RIGHT;
    if(this.y >= player.getY()) dir2 = UP;
    if(this.y < player.getY()) dir2 = DOWN;
    if ((int)random(0,2) == 1) // 50%
      return dir1;
    else
      return dir2;
  }

  public void hurt(int dmg) {
    this.hp -= dmg;
    if (this.hp <= 0) {
      die();
    }
  }

  public void die() {
    // possible other stuff, eg drop items
    this.markForDespawn(true);
  }

  public void drawIt() {
   // fill(255, 0, 0);
    //ellipse(this.getX(), this.getY(), this.getWidth(), this.getHeight());

    this.animationCounter = graphicsManager.showBombingBossImage(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    showHp(SHOW_BOSS_ENEMY_HP, BOMBING_FLYBOSS_HP);
  }
  
}
