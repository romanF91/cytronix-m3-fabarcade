/* displayed when changing the level */
class LevelEffect extends Effect {
  
  public LevelEffect () {
    super(true);
  }
  
  public void drawIt() {
    frame+=1;
    if (frame == 30) this.over = true;
    fill (0,0,0,20);
    rect (0,0,WINDOW_WIDTH,WINDOW_HEIGHT);
  }

}
