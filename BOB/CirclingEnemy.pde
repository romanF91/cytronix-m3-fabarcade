class CirclingEnemy extends Enemy{
  
  public static final int CIRCLING_SPEED = 2;
  public static final int CIRCLING_ENEMY_DMG = 1;
  public static final int CIRCLING_ENEMY_HP = STANDARD_ENEMY_HP;
  
  public final int[] UP_RIGHT = {UP, RIGHT};
  public final int[] RIGHT_DOWN = {RIGHT, DOWN};  
  public final int[] DOWN_LEFT = {DOWN, LEFT};  
  public final int[] LEFT_UP = {LEFT, UP};
  public final int[][] CIRCLE_DIRECTIONS = {UP_RIGHT, RIGHT_DOWN, DOWN_LEFT, LEFT_UP};
  public final int UP_RI = 0;
  public final int RI_DO = 1;
  public final int DO_LE = 2;
  public final int LE_UP = 3;
  
  private int changingSpeed = 4; // this effects the circles' radius
  private int currPercentage = 0; // this makes circlic movement possible. we dont have float values, but we can use chance
  private int currCircleDir = 0;
  
  public int getDmgGain() { return 1; }
  public int getHpGain()  { return STANDARD_ENEMY_HP / 2; }
  
  public CirclingEnemy(int x, int y) {
    super(x, y, 8, 8, CIRCLING_ENEMY_HP, CIRCLING_ENEMY_DMG, CIRCLING_SPEED);
    if(currLvl != null) this. setMaxHp(CIRCLING_ENEMY_HP+(calculateLevelFactor()*2));
    this.canFly = false;
    currCircleDir = (int)random(0,4);
  }
  
  public int getAttackDir() {
    return NOTHING; // No attack
  }
  
  public int movementAI(){ 
    
    // there is a chance for the second direction in the array to be selected
    int zero_or_one = (percentChance(currPercentage) ? 1 : 0);
    
    // which increases as time passes by
    this.currPercentage += this.changingSpeed;
    
    // at 100%, change direction:
    if (this.currPercentage > 100) {
      this.currPercentage = 0; // start new with the
      this.currCircleDir ++  ; // next 
      this.currCircleDir %= 4; // valid direction 
    }
    
    return CIRCLE_DIRECTIONS[currCircleDir][zero_or_one];
  }
  
  public void reactToObstacleCollision(int dir_from_where_collided){
    
    this.currPercentage = 0;
    if (dir_from_where_collided == DOWN)
      currCircleDir = DO_LE;
    if (dir_from_where_collided == LEFT)
      currCircleDir = LE_UP;
    if (dir_from_where_collided == UP)
      currCircleDir = UP_RI;
    if (dir_from_where_collided == RIGHT)
      currCircleDir = RI_DO;
  }
  
  
  
  public int calcSpeed() {
    return this.speed;
  }
  
  public void drawIt(){
    //fill(245, 10, 10);
    //ellipse(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    
    this.animationCounter = graphicsManager.showCirclingEnemyImage(this.getX() , this.getY(), this.calcSpeed(), this.getMoveDir(), animationCounter );
    showHp(SHOW_STANDARD_ENEMY_HP,this.getMaxHp());
  } 
}
