private class Animation {

  int frame = 0;
  int frameCounter = 0;
  int imgCounter   = 0;
  int width;
  float factor = 1;
  int oldDir = NOTHING;

  PImage[] images;

  public Animation(String dirPrefix, int count, int width) {
    this.imgCounter = count;
    images = new PImage[count];
    for (int i = 0; i < imgCounter; i++) {
      // Use nf() to number format 'i' into string digits
      String filename = dirPrefix + nf(i, 1) + ".png";
      images[i] = loadImage(filename);
    }

    this.width = width;
  }

  public int calcDisplayOffset(int c, int frame) {
    int offset;
    offset = images[frame].width - this.width;
    offset = (int) (offset*0.5);

    return offset;
  }

  public int display(int x, int y, int speed, int dir, int attackDir, int counter) {


    int displayOffsetX = calcDisplayOffset(x, frame);
    int startFrame = 0;
    if (dir != NOTHING) oldDir = dir;

    frameCounter       = counter;
    factor = speed*0.1*0.6;

    if (dir != NOTHING) {
      frameCounter++;

      if (this.imgCounter == 2) {
        startFrame = 0;
      }

      if (this.imgCounter == 8) {
        if (dir == LEFT) {
          startFrame = 0;
        }
        if (dir == UP || dir == UPLEFT || dir == UPRIGHT) {
          startFrame = 2;
        }
        if (dir == RIGHT) {
          startFrame = 4;
        }
        if (dir == DOWN || dir == DOWNLEFT || dir == DOWNRIGHT) {
          startFrame = 6;
        }
        // check attack directions
        if (attackDir == LEFT) {
          startFrame = 0;
        }
        if (attackDir == UP) {
          startFrame = 2;
        }
        if (attackDir == RIGHT) {
          startFrame = 4;
        }
        if (attackDir == DOWN) {
          startFrame = 6;
        }
      }


      //text(factor, 200, 100);
      //text(counter, 200,120);  // DEBUGGING
      //text(frame, 200, 140);

      if (frameCounter < frameRate*factor) frame = 0;
      else if (frameCounter >= frameRate*factor && frameCounter < frameRate*2*factor) frame = 1;
      else {
        frame = 0;
        frameCounter = 0;
      }

      image(this.images[startFrame + frame], x-displayOffsetX, y);
      
    } else { 

      if (this.imgCounter > 2) {
        if (attackDir != NOTHING) oldDir = attackDir;
        if (oldDir == LEFT) {
          startFrame = 0;
        }
        if (oldDir == UP) {
          startFrame = 2;
        }
        if (oldDir == RIGHT) {
          startFrame = 4;
        }
        if (oldDir == DOWN) {
          startFrame = 6;
        }
      } else {
        startFrame = 0;
      }
      image(this.images[startFrame], x-displayOffsetX, y);
    }

    return frameCounter;
  }
}

