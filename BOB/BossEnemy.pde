abstract class BossEnemy extends Enemy{
  int stages; 
    
  public BossEnemy(int x, int y, int width, int height, int hp, int damage, int speed){
    super(x,y,width,height,hp, damage, speed);
  } 
  
  public void dropItems(){
    generateItem(true, this.x, this.y);
  }
}
