abstract class Projectile extends WObject implements Movable, DealsDmg {
  int speed;
  int damage;
  int range;
  int distance;
  int direction;
  boolean attacksPlayer;
  
  public Projectile(int x, int y, int width, int height, int direction, boolean b) {
    super(x, y, width, height);
    this.direction = direction;
    this.attacksPlayer = b;
    this.distance = 0;
  }
  
  abstract void updatePosition();
  
  public int getDmg(){
     return damage; 
  }
  
  public boolean getAttacksPlayer(){
    return this.attacksPlayer; 
  }
  
  public void collision(WObject o){
    if(((o instanceof Player) && attacksPlayer) || ((o instanceof Enemy) && !attacksPlayer) || (o instanceof Obstacle)) {
     markForDespawn(true);
    }
  }
}
