/* displayed when changing the room */
class RoomEffect extends Effect {
  
  public RoomEffect () {
    super(true);
  }
  
  public void drawIt() {
    frame+=1;
    if (frame == 15) this.over = true;
    fill (0,0,0,40);
    rect (0,0,WINDOW_WIDTH,WINDOW_HEIGHT);
  }

}
