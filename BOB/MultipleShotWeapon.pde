class MultipleShotWeapon extends Weapon {
  
  public MultipleShotWeapon(int x, int y, int width, int height, int level, boolean b) {
    super(x, y, width, height, level, b);
    this.setName("MULTI SHOT");
    this.INIT_CHARGES = 20;
    this.INIT_COOLDOWN = 400;
    this.CD_BUFF_PER_LEVEL = 0;
    this.CHARGES_BUFF_PER_LEVEL = 5;
    this.recharge();
  }
  
  public void activate(){
    // nothing
  }
  
  // these startX and startY should now be the coordinates which are calculated correctly in the creature class
  protected void spawnProjectiles(int startX, int startY, int dir){
    
    if (dir != UP && dir != DOWN && dir != LEFT && dir != RIGHT) throw new Error("Weapon Shooting Error");
    
    int shoots = this.getLevel() + 1 ;
    //int SPACE_BETWEEN = (this.MAX_LEVEL * 5 - shoots * 5) + 1 ;
    int SPACE_BETWEEN = 1;
    int total_attack_width = ITEM_WIDTH * shoots + SPACE_BETWEEN * (shoots - 1);
    int single_attack_width = ITEM_WIDTH + SPACE_BETWEEN;
    
    if (dir == UP || dir == DOWN) {
      int attack_x_0 = startX - (int) (total_attack_width / 2);
      int attack_y_0 = startY;
      for (int i = 0; i < shoots; i++) {
        currRoom.getObjects().add(new PrototypeProjectile(attack_x_0 + i * single_attack_width, attack_y_0, dir, false));
      }
    } else if (dir == LEFT || dir == RIGHT) {
      int attack_x_0 = startX;
      int attack_y_0 = startY - (int) (total_attack_width / 2);
      for (int i = 0; i < shoots; i++) {
        currRoom.getObjects().add(new PrototypeProjectile(attack_x_0, attack_y_0 + i * single_attack_width, dir, false));
      }
    }
    soundManager.playShootSound();
  }
  
  public void drawIt() {
    fill(148, 0, 211);
    rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
  }
  
  protected int getInitCharges(){
    return INIT_CHARGES;
  }
  
}
