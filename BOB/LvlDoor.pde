class LvlDoor extends Door{
   public LvlDoor(int x, int y){
      super(x,y,NOTHING,true,false);
   } 
   
   public void loadNextLvl(){
      soundManager.stopMusic();
      
      int newLvlNumber = currLvl.getNumber()+1;
      int numberOfRooms = currLvl.getRoomCounter()+1;
      int newLevelType = (currLvl.getLevelType()%5)+1;  // calculates level type (for us there are 5)
      if(numberOfRooms > 25) numberOfRooms = 25;        // it is possible to fill the map completely (25 rooms) maybe this ist quite too much
     
      //create new Lvl
      currLvl = new Level(new Room(false), numberOfRooms, newLevelType,newLvlNumber);
      currLvl.addRooms();
      currRoom  = currLvl.getStartRoom();
      currRoom.setCurrRoom(true);
      
      currLvl.getStartRoom().getObjects().add(player);
      player.setX(ROOM_WIDTH/2-4);
      player.setY(ROOM_HEIGHT/2+HUD_HEIGHT-8);
   }
}
