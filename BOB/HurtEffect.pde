/* displayed when changing the level */
class HurtEffect extends Effect {
  
  int[] values = {40,70,100,70,45,25,15};
  
  public HurtEffect() {
    super(false);
  }
  
  public void drawIt() {
    if (frame == values.length-1) this.over = true;
    // fill (250, 0, 0, values[frame]);
    // rect (0,HUD_HEIGHT,WINDOW_WIDTH,WINDOW_HEIGHT-HUD_HEIGHT);
    graphicsManager.showHurtImage(values[frame]);
    frame+=1;
  }
}
