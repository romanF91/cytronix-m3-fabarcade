class PrototypeProjectile extends Projectile {
  
  int dir;
  
  public PrototypeProjectile(int x, int y, int direction, boolean b) {
    super(x, y, ITEM_WIDTH, ITEM_HEIGHT, direction, b);
    this.speed  = 3;
    this.damage = 1;
    this.range  = 45;
    this.dir = direction;
    this.attacksPlayer = b;
    
  }
  
  public void updatePosition(){
    if(this.direction == UP) this.y-= speed;  //move up
    if(this.direction == LEFT) this.x-= speed;  //move left
    if(this.direction == DOWN) this.y+= speed;  //move down
    if(this.direction == RIGHT) this.x+= speed;  //move right
    distance++;
    if(distance > range || this.x < 0 || this.x+this.getWidth() > ROOM_WIDTH || this.y < HUD_HEIGHT || this.y+this.getHeight() > ROOM_HEIGHT+HUD_HEIGHT){
       markForDespawn(false); 
    } 
  }  
  
  public void drawIt(){
      if(attacksPlayer) graphicsManager.showEnemyBasicShot(this.getX(),this.getY(), this.getWidth(), this.getHeight(), dir);
      else graphicsManager.showPlayerBasicShot(this.getX(),this.getY(), this.getWidth(), this.getHeight(), dir);
  }
}
