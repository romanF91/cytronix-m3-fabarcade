class RechargeItem extends Item {
  public RechargeItem(int x, int y, int width, int height) {
    super(x, y, width, height);
  }
  
  public void activate() {
    // recharges all weapons (do we want to keep that? now it is a mighty item.
    // we could introduce weapon recharge items for every weapon..)
    for(int i = player.getWeapons().size(); i > 0; i--) {
      player.getCurrWeapon().recharge();
      player.switchWeapon(1);
    }
  }
  
  public void drawIt() {
    //fill(139, 69, 19);
    //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    
    graphicsManager.showPickupAmmo(this.getX(), this.getY());
  }
}
