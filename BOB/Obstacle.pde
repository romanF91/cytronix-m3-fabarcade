abstract class Obstacle extends StaticObject {
  public Obstacle(int x, int y, int width, int height) {
    super(x, y, width, height);
  }
}
