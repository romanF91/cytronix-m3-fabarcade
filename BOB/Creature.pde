abstract class Creature extends WObject implements Movable {
  int hp;
  int maxHP;
  int level;  
  int damage;
  private int startTime = 0;      // time last attack started
  boolean canFly = false;
  private int currWeaponPos = 0;
  private ArrayList<Weapon> weapons;

  // these 4 allow collision with stones detection for enemys
  public boolean moveUp = false;
  public boolean moveRight = false;
  public boolean moveDown = false;
  public boolean moveLeft = false;
  // the following 4 have no use in enemy yet (but could be found useful later, i think)
  public boolean attackUp = false;
  public boolean attackRight = false;
  public boolean attackDown = false;
  public boolean attackLeft = false;



  public Creature(int x, int y, int width, int height, int hp, int damage) {
    super(x, y, width, height);
    this.hp = hp;
    this.damage = damage;
    this.maxHP = hp;
    this.weapons = new ArrayList<Weapon>();
  }

  public int getMoveDir() {
    int dir = NOTHING;

    if (this.moveUp)    dir = UP;
    if (this.moveDown)  dir = DOWN;
    if (this.moveLeft)  dir = LEFT;
    if (this.moveRight) dir = RIGHT;
    if (this.moveUp && this.moveLeft)   dir = UPLEFT;
    if (this.moveUp && this.moveRight)  dir = UPRIGHT;
    if (this.moveDown && this.moveLeft) dir = DOWNLEFT;
    if (this.moveDown && this.moveRight)dir = DOWNRIGHT;

    return dir;
  }
  
  public boolean canFly(){
     return this.canFly; 
  }

  public void setDirsToOnly(int dir) {

    if (dir == RIGHT) {
      moveUp = false;
      moveRight = true;
      moveDown = false;
      moveLeft = false;
    } 
    else if (dir == LEFT) {
      moveUp = false;
      moveRight = false;
      moveDown = false;
      moveLeft = true;
    } 
    else if (dir == UP) {
      moveUp = true;
      moveRight = false;
      moveDown = false;
      moveLeft = false;
    } 
    else if (dir == DOWN) {
      moveUp = false;
      moveRight = false;
      moveDown = true;
      moveLeft = false;
    } 
    else if (dir == NOTHING) {
      moveUp = false;
      moveRight = false;
      moveDown = false;
      moveLeft = false;
    }
    else if (dir == UPLEFT){
      moveUp = true;
      moveRight = false;
      moveDown = false;
      moveLeft = true; 
    }
    else if (dir == UPRIGHT){
      moveUp = true;
      moveRight = true;
      moveDown = false;
      moveLeft = false; 
    }
    else if (dir == DOWNLEFT){
      moveUp = false;
      moveRight = false;
      moveDown = true;
      moveLeft = true; 
    }
    else if (dir == DOWNRIGHT){
      moveUp = false;
      moveRight = true;
      moveDown = true;
      moveLeft = false; 
    }
  }


  public abstract int getAttackDir();
  public abstract void possibleHurt(int dmg); // this checks for timer cooldown before hurting
  public abstract void die();
  public abstract int calcSpeed(); // not getSpeed! calcSpeed! Player-Speed will include possible SpeedBuff!

  /*
   * implements obstacleCollision
   * \param o: the object that the creature has collided with
   * \return from where the creature ran into the object
   */
  public int obstacleCollision(WObject o) {

    int dir=NOTHING;
    int dir2=NOTHING;

    float xOverlap = 0;
    float yOverlap = 0;
    float difference = 0;
    int x1 = this.x;
    int y1 = this.y;
    int x2 = x1 + this.getWidth();
    int y2 = y1 + this.getHeight();
    int objX1 = o.getX();
    int objY1 = o.getY();
    int objX2 = objX1 + o.getWidth();
    int objY2 = objY1 + o.getHeight();
    
    // DEBUGGING TEXT
    //text(objX1, 10, 200);
    //text(objX2, 10, 220);
    //text(x1, 40, 200);
    //text(x2, 40, 220);

    // wenn ein x wert kollidiert
    if ((x1 >= objX1 && x2 <= objX2) || (x1 <= objX1 && x2 >= objX2) ) {
      // liegt die gesamte Breite im Objekt
      xOverlap = this.getWidth();
    }else{
      //rechter x wert
      if (x2 >=  objX1 && x1 <= objX1) {
        xOverlap = x2 - objX1;
        dir = LEFT;
      }
      //linker x wert
      if (x1 >= objX1 && x2 >= objX2) {
        xOverlap = objX2 - x1;
        dir = RIGHT;
      }
    }

    // wenn ein y wert kollidiert
    if ((y1 >= objY1 && y2 <= objY2) ) { 
      //liegt die gesamte Breite im Objekt
      yOverlap = this.getHeight();
    }else{
      //unterer y wert
      if (y2 >= objY1 && y1 <= objY1) { 
        yOverlap = y2 - objY1;
        dir2 = UP;
      }
      //oberer y wert
      if (y1 >= objY1 && y2 >= objY2) {
        yOverlap = objY2 - y1;
        dir2 = DOWN;
      }
    }
    difference = xOverlap - yOverlap;

    // DEBUGGING TEXT
    //text(difference, 50,50);
    //text(xOverlap, 50,70);
    //text(yOverlap, 50,90);

    if (difference > 0) { 
      if (dir2 == DOWN) this.y = objY2+1;
      if (dir2 == UP  ) this.y = objY1-this.getHeight()-1;
      dir = dir2;
    }
    else if (difference < 0) {
      if (dir == RIGHT) this.x = objX2+1;
      if (dir == LEFT)  this.x = objX1-this.getWidth()-1;
    }  
    else {
      if (dir2 == DOWN && dir == LEFT){
        this.y = objY2+1;
        this.x = objX1-this.getWidth()-1;
      }
      if (dir2 == DOWN && dir == RIGHT){
        this.y = objY2+1;
        this.x = objX2+1;
      }
      if (dir2 == UP   && dir == LEFT) {
        this.y = objY1-this.getHeight()-1;
        this.x = objX1-this.getWidth()-1;
      }
      if (dir2 == UP   && dir == RIGHT){
        this.y = objY1-this.getHeight()-1;
        this.x = objX2+1;
      }
    }
    
    // DEBUGGING TEXT
    //text(dir, 50,50);
    //text(dir2, 50,70);
    //text(yOverlap, 50,90);
    return dir;
  }   


  /** 
   * This Method does the basic updating position stuff.
   * It gets used by the Player class.
   * The enemy class overrides it, but calls this routine as well.
   * Therefore, enemys have opportunity to change their move-Booleans before calling this.
   */
  public void updatePosition() {

    // note that this variable overides this.speed for the duration of the method due to same name
    int speed = this.calcSpeed();

    // move me!
    if (this.moveUp)     this.y-= speed;  //move up
    if (this.moveRight)  this.x+= speed;  //move rightg
    if (this.moveDown)   this.y+= speed;  //move down
    if (this.moveLeft)   this.x-= speed;  //move left

    // keep me in the room
    if (this.x<=0) {
      x=0;
      if (this instanceof Enemy) ((Enemy)this).reactToObstacleCollision(RIGHT);
    }    
    if (this.x>=currRoom.getWidth() - width) {
      x = currRoom.getWidth()-width;
      if (this instanceof Enemy) ((Enemy)this).reactToObstacleCollision(LEFT);
    }
    if (this.y<HUD_HEIGHT) {
      y = HUD_HEIGHT;
      if (this instanceof Enemy) ((Enemy)this).reactToObstacleCollision(DOWN);
    }    
    if (this.y>= HUD_HEIGHT + currRoom.getHeight() - height) {
      y = HUD_HEIGHT + currRoom.getHeight()-height;
      if (this instanceof Enemy) ((Enemy)this).reactToObstacleCollision(UP);
    }
  }

  public void possibleAttack() {

    int attackDir = this.getAttackDir();
    // creates a new projectile if attack key is pressed and delay is over
    if (attackDir != NOTHING && (startTime + this.getCurrWeapon().getCooldown() < millis())) {
      // ATTACK!
      startTime = millis();
      this.attack(attackDir);
    }
  }

  public void attack(int dir) {
    this.getWeapons().get(this.getCurrWeaponPos()).shoot(getStartProjPosX(), getStartProjPosY(), dir);
  }  

  public int getStartProjPosX() {

    if      (attackLeft)          return this.x - ITEM_WIDTH;
    else if (attackRight)         return this.x +  this.width;
    else   /* attack up/down   */ return this.x + (this.width/2) - ITEM_WIDTH/2;
  }

  public int getStartProjPosY() {

    if      (attackUp)            return this.y - ITEM_HEIGHT;
    else if (attackDown)          return this.y +  this.height;
    else   /* attack rightleft */ return this.y + (this.height/2) - ITEM_HEIGHT/2;
  }

  public int getHp() {
    return this.hp;
  }

  public void setHp(int hp) {
    this.hp = hp;
  }

  public int getMaxHp() {
    return this.maxHP;
  }

  public void setMaxHp(int hp) {
    this.maxHP = hp; 
    this.setHp(maxHP);
  }

  public Weapon getCurrWeapon() {
    return this.getWeapons().get(this.currWeaponPos);
  }

  public ArrayList<Weapon> getWeapons() {
    return this.weapons;
  }

  public int getCurrWeaponPos() {
    return this.currWeaponPos;
  }

  public void setCurrWeaponPos(int x) {
    this.currWeaponPos = x;
  }


  public void hurt(int dmg) {
    this.hp -= dmg;
    if (this.hp <= 0) {
      this.hp = 0;
      this.die();
    }
  }
}

