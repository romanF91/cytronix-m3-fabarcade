class Intro{
 
 int imageCounter = 0;
 int DELAY[] = {6000,8200,1200,1200,2000,8800,500,10700};
 int START_TIME = 2000;
 PImage[] images;
 boolean FINISHED = false;
 boolean musicPlayed = false;
 float transparency = 0;
 boolean more = true;
 float color1 = 0;
 float color2 = 0;
 
 public Intro(){
     //---
     this.images = graphicsManager.getIntroImages();
 }
 
 public void displayImage(int i){
    textAlign(CORNER);
    textFont(hud);
    textSize(9);
    fill(255);
    
   
    if(i == 0){
     
     background(0);
     if (transparency > 255) more = false;
     if (transparency < 0  ) {
       //more = true;
       transparency = 0;
     }
     if (more) { transparency += 1.3; }
     else transparency -= 1.3;
     tint(255, transparency);
     image(images[i],0,0);
    } 
    
    if(i > 1 ){
      image(images[i],0,0);
    }  
    
    
    if(i == 1){
       if(!musicPlayed){
         background(0);
         soundManager.playMenuMusic();
         musicPlayed = true;
         transparency = 0;
       }
       if (more && transparency < 255) { transparency += 0.5; }
       tint(255,transparency);
       image(images[i],0,0);
       
       if(START_TIME + 2000 < millis()){
         if (color1 < 255) { color1 += 2; }
         fill(color1);
         text("YEAR 2000 -", 19,19);
       }

       if(START_TIME +4500 < millis()){
          
         if(color2 < 255) { color2 += 2; }
         fill(color2);         
         text("EARTH WAS OVERRUN", 19,39);
         text("BY AN EXTRATERRESTRIAL POWER.",19,59);
       }
    }
    if(i == 2){
       color1 = 0;
       color2 = 0; 
    }
    if(i == 5){
       if(START_TIME + 500 < millis()){
         
         if (color1 < 255) { color1 += 2; }
         fill(color1);
         text("THE HUMAN SURVIVORS ESCAPED", 9,149);
         text("WITH THE HELP OF SPACESHIPS.", 9,169);
         text("ON A RESEARCH SHIP, COMMANDER ",9,189);
         text("JACK'S MISSION IS TO STUDY THE",9,209);
         text("ENEMY SPECIES.",9,229);
       }
    }
    if(i == 6){
       color1 = 0; 
    }
    if(i == 7){
       if(START_TIME + 0 < millis() && START_TIME+9500 > millis()){
         
         if (color1 < 255) { color1 += 2; }
           fill(color1);
           text("SUDDENLY SOMETHING HAPPENED IN", 9,149);
           text("THE LABORITORY.", 9,169);
           text("NOW THERE IS ONLY ONE MISSION LEFT:", 9,189);       
       }
    
       if(START_TIME + 9500 < millis()){
         
        fill(255);
        textAlign(CENTER);
        textSize(32);
        text("SURVIVE!", WINDOW_WIDTH/2,WINDOW_HEIGHT*0.8);
       }
    }
 }
 
 public void play(){

     if(START_TIME + DELAY[imageCounter] > millis()){
       displayImage(imageCounter);
     }else{
        START_TIME = millis();
        imageCounter++;
        more = true;
        if(imageCounter >7) {
          imageCounter = 7;
          setFinished(true);
        }
     }
 }
 
 public void setFinished(boolean b){
    FINISHED = b;
    noTint(); 
 }
 
}
