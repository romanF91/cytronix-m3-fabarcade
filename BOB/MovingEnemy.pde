class MovingEnemy extends Enemy{
  
  public static final int MOVING_ENEMY_SPEED = 1;
  public static final int MOVING_ENEMY_DMG = 1;
  public static final int MOVING_ENEMY_HP = STANDARD_ENEMY_HP;
  
  public int getDmgGain() { return 1; }
  public int getHpGain()  { return 1; }
  
  int dir = 2;
  int attackDir = 0;
  
  private int startMoveTime = 0; 
  private int movingCD = 600;     // delay till direction change
  
  public MovingEnemy(int x,int y){
    super(x,y,16,16,MOVING_ENEMY_HP, MOVING_ENEMY_DMG, MOVING_ENEMY_SPEED);
    if(currLvl != null) this. setMaxHp(MOVING_ENEMY_HP+(calculateLevelFactor()*2));
    this.cooldown = 400;
  }
  
  public int getAttackDir(){
  /*
  This is old code. Do we want Moving Enemys to attack?
    if(player != null){
      if(this.y > player.getY()) attackDir = 1;
      if(this.x > player.getX()) attackDir = 2;
      if(this.y < player.getY()) attackDir = 3;
      if(this.x < player.getX()) attackDir = 4;
    }
  */
    return NOTHING;
  }
  
  public int movementAI(){
    if(startMoveTime + movingCD < millis()){
      startMoveTime = millis();
      dir = (int)random(0,4);
    }
    return dir;
  }
  
  public void reactToObstacleCollision(int dir_from_where_collided) {
    this.dir = dir_from_where_collided;
  }
  
  public void die(){
    // possible other stuff, eg drop items
     this.markForDespawn(true);
  }
  
  public void drawIt(){
    //fill(255, 0, 0);
    //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    this.animationCounter = graphicsManager.showMovingEnemyImage(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    showHp(SHOW_STANDARD_ENEMY_HP,this.getMaxHp());
  }
}
