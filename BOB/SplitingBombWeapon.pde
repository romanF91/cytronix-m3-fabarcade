class SplitingBombWeapon extends Weapon {
  
  public SplitingBombWeapon(int x, int y, int width, int height, int level, boolean b) {
    super(x, y, width, height, level, b);
    this.setName("BOMB");
    this.attacksPlayer = b;
    this.INIT_CHARGES = 6;
    this.INIT_COOLDOWN = 450;
    this.CD_BUFF_PER_LEVEL = 0;
    this.CHARGES_BUFF_PER_LEVEL = 4;
    this.recharge();
  }
  
  public void activate(){
    for(int i= currRoom.getObjects().size()-1; i>=0; i--) {
      if(currRoom.getObjects().get(i) instanceof SplitingBombProjectile){
        if(((SplitingBombProjectile)currRoom.getObjects().get(i)).getConnectedWeapon() == this){ 
          ((SplitingBombProjectile)currRoom.getObjects().get(i)).bombExplode();
          currRoom.getObjects().remove(i);
        }      
      }
    }
  }
  
  
  
  // these startX and startY should now be the coordinates which are calculated correctly in the creature class
  protected void spawnProjectiles(int startX, int startY, int dir){
    
    if (dir != UP && dir != DOWN && dir != LEFT && dir != RIGHT) throw new Error("Weapon Shooting Error");
    SplitingBombProjectile proj = new SplitingBombProjectile(startX, startY, dir, this.attacksPlayer, this);
    currRoom.getObjects().add(proj);
    if(this.attacksPlayer){
      this.INIT_COOLDOWN = 1000;
      this.recharge();
    }
    soundManager.playPlantBombSound();
  }
  
  public void drawIt() {
    fill(0, 0, 0);
    rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
  }
  
  protected int getInitCharges(){
    return INIT_CHARGES;
  }
  
}
