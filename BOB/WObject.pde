abstract class WObject {
  int x;
  int y;
  int width;
  int height;
  boolean despawn;
  boolean despawnWay;
  int animationCounter = 0;
  
  public WObject(int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.despawn = false;
  }
  
  /*
   * Marks a World-Object for despawning as soon as possible.
   * \param way A boolean specifying the way of despawning (eg exploding or simply disappearing)
   */
  public void markForDespawn(boolean way) {
    this.despawn = true;
    this.despawnWay = way;
  }
  
  public void despawn() {
    if(this instanceof Enemy) ((Enemy)this).dropItems();
    currRoom.getObjects().remove(this);
  }

  public int getX() {
    return x;
  }
  public int getY() {
    return y;
  }
  public void setX(int x) {
    this.x = x;
  }
  public void setY(int y) {
    this.y = y;
  }
  public int getWidth() {
    return width;
  }
  public int getHeight() {
    return height;
  }
  
  public abstract void drawIt();
}
