class Highscore {
  private String file;
  private ArrayList<Entry> entries;
  private Highscore(String file) {
    entries = new ArrayList<Entry>();
    this.file = file;
    String[] rows;
    rows = loadStrings(file);
    for(int i = 0; i< rows.length; i++) {
      if(!rows[i].isEmpty()) {
        String[] s = rows[i].split("#");
        entries.add(new Entry(s[1],Integer.parseInt(s[0])));
      }
    }
  }  
  public boolean inHighscore(int level) {
    return entries.size() < 10 || entries.get(9).getLevel() < level;
  }
  public void insert(String name,int level) {
    int index = 0;
    for(int i=0;i < entries.size(); i++) {
      if(entries.get(i).getLevel() >= level) {
        index = i;
      }
    }   
    entries.add(index,new Entry(name,level));       
    if(entries.size() > 10) entries.remove(10);
    
    saveHighscore();
  }
  public void saveHighscore() {
    String[] rows = new String[10];
    int i;
    for(i=0; i< entries.size(); i++) {
      rows[i] = entries.get(i).getLevel() + "#" + entries.get(i).getName();
    }
    for(; i<10; i++) {
      rows[i] = ""; 
    }
    saveStrings("data/" + file,rows);
  }

  public String getEntryName(int i) {
     return entries.get(i).getName(); 
  }
  public String getEntryLevel(int i) {
     return "" + entries.get(i).getLevel(); 
  }
  public int getEntryCount() {
     return entries.size(); 
  }
  
  
  class Entry {
    private int level;
    private String name; 
    public Entry(String name, int level) {
      this.name = name;
      this.level = level;
    }
    public int getLevel() {
      return level;
    }
    public String getName() { 
      return name;
    }  
  }
}
