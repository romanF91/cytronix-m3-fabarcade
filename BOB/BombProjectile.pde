class BombProjectile extends Projectile {
  
  public BombProjectile(int x, int y, int direction, boolean b) {
    super(x, y, ITEM_WIDTH, ITEM_HEIGHT, direction, b);
    this.speed  = 3;
    this.damage = 2;
    this.range  = 100;
  }
  
  public void updatePosition(){
    if(this.direction == UP) this.y-= speed; 
    if(this.direction == LEFT) this.x-= speed;  
    if(this.direction == DOWN) this.y+= speed; 
    if(this.direction == RIGHT) this.x+= speed; 
    if(this.direction == UPLEFT){  
      this.y-= speed;  
      this.x-= speed;
    }
    if(this.direction == DOWNLEFT){
      this.y+= speed;  
      this.x-= speed;
    }
    if(this.direction == UPRIGHT){
      this.y-= speed;  
      this.x+= speed;
    } 
    if(this.direction == DOWNRIGHT){
      this.y+= speed;  
      this.x+= speed;
    }
    distance++;
    if(distance > range || this.x < 0 || this.x+this.getWidth() > ROOM_WIDTH || this.y < HUD_HEIGHT || this.y+this.getHeight() > ROOM_HEIGHT+HUD_HEIGHT){
       markForDespawn(false); 
    } 
  }  
  
  public void drawIt(){
    graphicsManager.showBombProjectile(this.x, this.y, ITEM_WIDTH, ITEM_HEIGHT);
  }
}
