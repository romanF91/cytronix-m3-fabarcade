class BubbleWeapon extends Weapon {
  
  public BubbleWeapon(int x, int y, int width, int height, int level, boolean b) {
    super(x, y, width, height, level, b);
    this.setName("BUBBLE GUN");
    this.INIT_CHARGES = 20;
    this.INIT_COOLDOWN = 210;
    this.CD_BUFF_PER_LEVEL = 40;
    this.CHARGES_BUFF_PER_LEVEL = 10;
    this.recharge();
  }
  
  public void activate(){
    // nothing
  }
  
  // these startX and startY should now be the coordinates which are calculated correctly in the creature class
  protected void spawnProjectiles(int startX, int startY, int dir){
    
    if (dir != UP && dir != DOWN && dir != LEFT && dir != RIGHT) throw new Error("Weapon Shooting Error");
    
    currRoom.getObjects().add(new BubbleProjectile(startX, startY, dir, false));
    
    soundManager.playBubbleSound();
  }
  
  public void drawIt() {
    fill(20, 20, 200);
    rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
  }
  
}
