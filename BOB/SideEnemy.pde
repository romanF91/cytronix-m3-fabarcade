class SideEnemy extends Enemy{
  
  public static final int SIDE_ENEMY_SPEED = 1;
  public static final int SIDE_ENEMY_DMG = 1;
  public static final int SIDE_ENEMY_HP = STANDARD_ENEMY_HP;
  
  public int getDmgGain() { return 1; }
  public int getHpGain()  { return 2; }
  
  int dir = 0;

  public SideEnemy(int x,int y){
    super(x,y,16,16,SIDE_ENEMY_HP,SIDE_ENEMY_DMG,SIDE_ENEMY_SPEED);
    this.cooldown = 400;
    this.getWeapons().add(new BasicAttack(0, 0, ITEM_WIDTH, ITEM_HEIGHT, 1, true));
  }
  
  public int getAttackDir() {
    int attackDir=UP;
    if(player != null) {
      if(this.y < player.getY()) attackDir = DOWN;
    }
    return attackDir;  
  }

  public int movementAI(){
    if(this.x<=0) this.dir=RIGHT;
    if(this.x>=currRoom.getWidth()-this.getWidth()) this.dir=LEFT;
    return this.dir;
  }  
  
  public void reactToObstacleCollision(int dir_from_where_collided) {
    this.dir = dir_from_where_collided;
  }
  
  public void drawIt(){
    fill(255, 0, 0);
    //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    this.animationCounter = graphicsManager.showSideEnemyImage(this.getX(), this.getY(), this.calcSpeed(), this.dir, animationCounter);
    
    
    showHp(SHOW_STANDARD_ENEMY_HP,this.getMaxHp());
  }
}
