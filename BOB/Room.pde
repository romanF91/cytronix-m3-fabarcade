class Room {
  private ArrayList<WObject> objects;
  private ArrayList<Door> doors;
  private int width = ROOM_WIDTH;
  private int height = ROOM_HEIGHT;
  private boolean currentRoom; 
  private boolean completed;
  private boolean isBossRoom;
  
  public Room(boolean b) {
    objects = new ArrayList<WObject>();
    this.completed = false;
    this.isBossRoom = b;
    lockDoors();
  }
  
  public void setCurrRoom(boolean b){
    this.currentRoom = b; 
  }
  
  public boolean isCurrRoom(){
    return this.currentRoom; 
  }
  
  
  public boolean spawn(String s, int number){
    boolean success = true;
    boolean collision = true;
    int roomWidthMid = ROOM_WIDTH/2;
    int roomHeightMid = ROOM_HEIGHT/2;
    
    int left = 0;
    int right = ROOM_WIDTH-50;
    int top = HUD_HEIGHT+20;
    int bot = ROOM_HEIGHT-20;
    
    for(int i = 0; i<number; i++){ 
     if(s == "Player")
       this.spawn(s,number,ROOM_WIDTH/2, HUD_HEIGHT + ROOM_HEIGHT/2);
     else if(s == "Fly" || s == "MovingEnemy" || s == "SideEnemy" || s == "CirclingEnemy" || s == "HPItem" || s == "RechargeItem" || s == "AntiFly") {
       this.spawn(s,1,(int)random(left, right), (int)random(top, bot));
     }
     else if(s == "MovingBoss" | s == "BombingFlyBoss" || s=="SplittingBoss")
       this.spawn(s,1,roomWidthMid, roomHeightMid);
     else if(s == "Stone"){
       int STONE_SIZE = 26;
       int obsX = (STONE_SIZE-6) * (int) random(0, ROOM_WIDTH/(STONE_SIZE-6));
       int obsY = STONE_SIZE * (int) random(0, ROOM_HEIGHT/STONE_SIZE);
       
       // don't spawn stones around doors or in the middle of the room
       if     ((obsX < 48 || obsX+STONE_SIZE > ROOM_WIDTH-48)  && obsY < roomHeightMid + 48 && obsY+STONE_SIZE > roomHeightMid - 48);// don't spawn stones around left and right door
       else if((obsY < 48 || obsY+STONE_SIZE > ROOM_HEIGHT-48) && obsX < roomWidthMid  + 48 && obsX+STONE_SIZE > roomWidthMid  - 48);// don't spawn stones around bottom and top door
       else if((obsX < roomWidthMid + 20 && obsX+STONE_SIZE > roomWidthMid - 20 && obsY < roomHeightMid + 20 && obsY+STONE_SIZE > roomHeightMid - 20));// don't spawn stones in the middle of the room
       else this.spawn(s,1,obsX,obsY+HUD_HEIGHT); // spawn stone!
     }
     else if (s == "test_item") {
       this.spawn(s,number,100,100);
       break; // yeah this function is somehow still not written in a nice way. whatever.. this works
     }
     else
       success = false;
    }
    return success;
  }
  public boolean spawn(String s, int number, int x, int y){
    boolean success = true;
    boolean collision = true;
    int roomWidthMid = ROOM_WIDTH/2;
    int roomHeightMid = ROOM_HEIGHT/2;
    
    int left = 0;
    int right = ROOM_WIDTH-50;
    int top = HUD_HEIGHT+20;
    int bot = ROOM_HEIGHT-20;
      
    for(int i = 0; i<number; i++){ 
      if(s == "Player")
        this.objects.add(new Player(ROOM_WIDTH/2, HUD_HEIGHT + ROOM_HEIGHT/2));
      else if(s == "Fly")
        this.objects.add(new Fly(x,y));
      else if(s == "AntiFly")
        this.objects.add(new AntiFly(x,y));
      else if(s == "MovingEnemy")
        this.objects.add(new MovingEnemy(x,y));
      else if(s == "SideEnemy")
        this.objects.add(new SideEnemy(x,y));
      else if(s == "CirclingEnemy")
        this.objects.add(new CirclingEnemy(x,y));
      else if(s == "MovingBoss")
        this.objects.add(new MovingBoss(x, y));
      else if(s == "BombingFlyBoss"){
        this.objects.add(new BombingFlyBoss(x, y));
      }
      else if(s == "SplittingBoss")
        this.objects.add(new SplittingBoss(x, y));
      else if(s == "SplittingBossAdd")
        this.objects.add(new SplittingBossAdd(x, y, 2));
      else if(s == "SplittingBossAdd2")
        this.objects.add(new SplittingBossAdd(x, y, 1));
      else if(s == "SplittingBossAdd3")
        this.objects.add(new SplittingBossAdd(x, y, 0));
      else if(s == "Stone"){
        int STONE_SIZE = 20;
        this.objects.add(new Stone(x,y,STONE_SIZE,STONE_SIZE)); // spawn stone!
      }
      else if(s == "HPItem")
        this.objects.add(new HPItem(x,y,ITEM_WIDTH,ITEM_HEIGHT));
      else if(s == "RechargeItem")
        this.objects.add(new RechargeItem(x,y,ITEM_WIDTH,ITEM_HEIGHT));
      else if(s == "test_item")
        this.objects.add(new BubbleWeapon(x,y+10*i,ITEM_WIDTH,ITEM_HEIGHT, 1, false));
      else
        success = false;
    }
     
    // compares spawn the positions between objects and obstacles and resets the position in case of collision
    while(collision){
      collision = false;
      for (WObject o1 : this.objects) {
        if (o1 instanceof Obstacle) {
          for (WObject o2 : objects) {
            if ( o2 instanceof Obstacle) continue; 
            if (collision(o1, o2)) {
              collision = true;
              o2.setX((int)random(0,ROOM_WIDTH-o2.getWidth()+1)); //+1 just to be on the save side ;)
            }
          }
        }
      }
    }     
    return success;
  }
  
  public void setCompleted(boolean b){
     this.completed = b; 
  }
  public boolean isCompleted(){
     return this.completed; 
  }
  
  public void checkCompletion(){
     int enemyCounter = 0;
     
     for (WObject o1 : this.getObjects()) { 
        if (o1 instanceof Enemy) enemyCounter++;
     }
     
     if(enemyCounter == 0) this.setCompleted(true);
     else this.setCompleted(false);
  }
  
  public void setBossRoom(boolean b){
     this.isBossRoom = b; 
  }
  
  public boolean isBossRoom(){
     return this.isBossRoom; 
  }
  
  public ArrayList<WObject> getObjects() {
    return objects;
  }
  
  public void setWidth(int w){
     this.width = w;
  }
  
  public void setHeight(int h){
     this.width = h; 
  }
  
  public int getWidth(){
     return this.width; 
  }
  
  public int getHeight(){
     return this.height; 
  }
  
  // adds door to room 
  public void addDoor(int position, boolean isBossDoor){
    if(position == RIGHT)
      this.objects.add(new Door(ROOM_WIDTH-16, (ROOM_HEIGHT/2) + HUD_HEIGHT-8, position, false, isBossDoor));
    if(position == LEFT)
      this.objects.add(new Door(0, ROOM_HEIGHT/2 + HUD_HEIGHT-8, position, false, isBossDoor));
    if(position == UP)
      this.objects.add(new Door(ROOM_WIDTH/2-8, HUD_HEIGHT, position, false, isBossDoor));
    if(position == DOWN)
      this.objects.add(new Door(ROOM_WIDTH/2-8, ROOM_HEIGHT + HUD_HEIGHT-16, position, false, isBossDoor));
  }  
  
  public void unlockDoors(){
     for(WObject o : this.objects){
        if(o instanceof Door)
          ((Door) o).open();
     } 
  }
  
  public void lockDoors(){
     for(WObject o : this.objects){
        if(o instanceof Door)
          ((Door) o).close();
     } 
  }
  
  public Player getPlayer () {
    // Gets player out of ObjectList 
    for (WObject o : currRoom.getObjects()) {
      if (o instanceof Player) return ((Player) o);
    }
    return null;
  }
}
