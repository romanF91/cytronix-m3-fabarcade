class SplittingBoss extends BossEnemy {
  
  public static final int SPLITTING_BOSS_HP = BOSS_ENEMY_HP;
  public static final int SPLITTING_BOSS_SPEED = 2;
  public static final int SPLITTING_BOSS_DMG = 3;
  
  public int getDmgGain() { return 2; }
  public int getHpGain()  { return BOSS_ENEMY_HP / 2; }

  int dir = 2;
  int attackDir = 0;

  private int startMoveTime = 0; 
  private int movingCD = 600;     // delay till direction change

  public SplittingBoss(int x, int y) {
    super(x, y, 64, 64, SPLITTING_BOSS_HP, SPLITTING_BOSS_DMG, SPLITTING_BOSS_SPEED);
    if(currLvl != null) this. setMaxHp(SPLITTING_BOSS_HP+(calculateLevelFactor()*5));
    this.stages =3;
    this.cooldown = 200;
  } 

  public int getAttackDir() {
    return NOTHING;
  }
  
  public void possibleAttack() {

    if (this.stages == 3 && (this.hp <= (int)(BOSS_ENEMY_HP*0.75))) {
      this.stages--;
      this.width = this.width/2;
      this.height = this.height/2;
      currRoom.spawn("SplittingBossAdd", 1, this.x, this.y);
    }
    if (this.stages == 2 && (this.hp <= (int)(BOSS_ENEMY_HP*0.5))) {
      this.stages--;
      this.width = this.width/2;
      this.height = this.height/2;
      currRoom.spawn("SplittingBossAdd2", 1, this.x, this.y);
    }
    if (this.stages == 1 && (this.hp <= (int)(BOSS_ENEMY_HP*0.25))) {
      this.stages--;
      this.width = this.width/2;
      this.height = this.height/2;
      this.speed++;
      currRoom.spawn("SplittingBossAdd3", 1, this.x, this.y);
    }
    
    super.possibleAttack();
    // in case we want to add attacks (in getAttackDir != NOTHING),
    // they wouldn't trigger if we forgot to do this.
  }

  public int movementAI(){  
    
    if(startMoveTime + movingCD < millis()){
      startMoveTime = millis();
      dir = (int)random(0,4);
    }
    return dir;
  }

  public void hurt(int dmg) {
    this.hp -= dmg;
    if (this.hp <= 0) {
      die();
    }
  }

  public void die() {
    // possible other stuff, eg drop items
    this.markForDespawn(true);
  }

  public void drawIt() {
    if(this.stages == 3) this.animationCounter = graphicsManager.showSplittingBossImage(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    if(this.stages == 2) this.animationCounter = graphicsManager.showSplittingBossAdd1Image(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    if(this.stages == 1) this.animationCounter = graphicsManager.showSplittingBossAdd2Image(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    if(this.stages == 0) this.animationCounter = graphicsManager.showSplittingBossAdd3Image(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    showHp(SHOW_BOSS_ENEMY_HP, SPLITTING_BOSS_HP);
  }
  public void reactToObstacleCollision(int dir_from_where_collided){
    dir = dir_from_where_collided;
  }
  
}

