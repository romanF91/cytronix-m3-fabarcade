class Fly extends Enemy{
  
  public static final int FLY_SPEED = 1;
  public static final int FLY_DMG = 1;
  public static final int FLY_HP = 3;
  
  public int getDmgGain() { return 1; }
  public int getHpGain()  { return 1; }
  
  public Fly(int x, int y){
    super(x, y, 9, 9, FLY_HP, FLY_DMG, FLY_SPEED);
    if(currLvl != null) this. setMaxHp(FLY_HP+(calculateLevelFactor()*2));
    this.canFly = true;
  }
  
  public int getAttackDir() {
    return NOTHING; // Flys don't attack
  }
  
  public int movementAI(){
    if (player == null) return NOTHING;
    int dir1 = NOTHING;
    int dir2 = NOTHING;
    if(this.x >= player.getX()) dir1 = LEFT;
    if(this.x < player.getX()) dir1 = RIGHT;
    if(this.y >= player.getY()) dir2 = UP;
    if(this.y < player.getY()) dir2 = DOWN;
    if ((int)random(0,2) == 1) // 50%
      return dir1;
    else
      return dir2;
  }
  
  public void reactToObstacleCollision(int dir_from_where_collided){
    // we dont do anything, because we go to 2 directions all the time anyways
  }
  
  
  
  public int calcSpeed() {
    return this.speed + (int)random(-1,1);
  }
  
  public void drawIt(){
    //fill(255, 0, 0);
    //ellipse(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    
    this.animationCounter = graphicsManager.showFlyEnemyImage(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), animationCounter);
    showHp(SHOW_STANDARD_ENEMY_HP,this.getMaxHp());
  } 
}
