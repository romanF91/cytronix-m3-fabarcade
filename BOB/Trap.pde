abstract class Trap extends StaticObject implements DealsDmg {
  
  private int damage;
  public Trap(int x, int y, int width, int height) {
    super(x, y, width, height);
  }
  
  public int getDmg(){
     return this.damage; 
  }
}
