class Player extends Creature {

  private double speedBuff = 1.0; // this is nicely implemented in case we want to introduce buffing items eg.
  private int hitcoolDown = 500;
  private int hitTime = 0;
  
  
  public Player(int x, int y) {
    super(x, y, 12, 25, PLAYER_HP, 0);
    this.getWeapons().add(new BasicAttack(0, 0, ITEM_WIDTH, ITEM_HEIGHT, 1, false));
  }
  
  public int getAttackDir() {
    int attackDir;
    if(attackUp) attackDir = UP;
    else if(attackLeft) attackDir = LEFT;
    else if(attackDown) attackDir = DOWN;
    else if(attackRight) attackDir = RIGHT;
    else attackDir = NOTHING;
    return attackDir;
  }

  public void possibleHurt(int dmg){
    if(hitTime + hitcoolDown < millis()){
      soundManager.playHurtSound();
      currEffect = new HurtEffect();
      hitTime = millis();
      this.hurt(dmg);
    }
  }
  
  public void die(){
     this.markForDespawn(true);
     soundManager.playGameOverSound();
     GAME_STATE = GAME_OVER;
  }
  
  public int calcSpeed(){
    return (int) (PLAYER_BASIC_SPEED * this.getSpeedBuff());  
  }

  public double getSpeedBuff() {
    return this.speedBuff;
  }
  
  /* only works with next = 1, otherwise possible undefined behaviour
   */
  public void switchWeapon(int next){
      ArrayList<Weapon> weapons = this.getWeapons();
      this.setCurrWeaponPos(this.getCurrWeaponPos() + next);
      if(this.getCurrWeaponPos() < 0) this.setCurrWeaponPos(this.getWeapons().size()-1);
      else if(this.getCurrWeaponPos() >= weapons.size()) this.setCurrWeaponPos(0);
  }
  
  /**
   * Sets the player speed buff. 1.0 returns it to normal.
   * @param speedBuffIn
   */
  public void setSpeedBuff(double speedBuffIn) {
    this.speedBuff = speedBuffIn;
  }
  
  public void drawIt(){
    //fill(255);
    //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    this.animationCounter = graphicsManager.showPlayerImage(this.getX(),this.getY(), this.getWidth(), this.getHeight(), this.getMoveDir(), this.getAttackDir(), animationCounter );
  }
  
  public void collision(WObject o){
    if(o instanceof DealsDmg && !(o instanceof Projectile && ((Projectile)o).attacksPlayer == false) && !(o instanceof SplitingBombProjectile)) {
      this.possibleHurt(((DealsDmg)o).getDmg());
    }
    
    if(o instanceof Door && ((Door) o).isOpen()) {
      currEffect = new RoomEffect();
      currDoor = (Door) o; 
    }
    
    if(o instanceof LvlDoor && ((LvlDoor)o).isOpen()){
      currEffect = new LevelEffect();
      currDoor = (LvlDoor) o;
    }
    
    if(o instanceof Item) {
      if (o instanceof Weapon) {
        Weapon newWeapon = this.existingWeaponOfSameType((Weapon) o);
        if (newWeapon == null) { // completely new Weapon!
          this.getWeapons().add((Weapon) o);
          this.setCurrWeaponPos(this.getWeapons().size()-1);
        } else {
          newWeapon.levelUp();
          // switch to improved weapon
          while (this.getCurrWeapon() != newWeapon)
            this.switchWeapon(1);
          this.getCurrWeapon().recharge();
        }
      }
      else {
        ((Item) o).activate();
        
      }
      soundManager.playPickupSound();
      o.markForDespawn(true);
    }
    
    if(o instanceof Obstacle) {
      this.obstacleCollision(o);
    }
  }
  
  /*
   * \param An object of type Weapon.
   * \return The first Weapon the player possesses which has the same type, or null if not found
   */
  public Weapon existingWeaponOfSameType(Weapon newWeapon) {
    for (Weapon w : this.getWeapons()) {
      if (w.getClass() == newWeapon.getClass()) return w;
    }
    return null;
  } 
}
