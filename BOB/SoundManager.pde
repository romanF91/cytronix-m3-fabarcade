class SoundManager{

  Process ingameMusic, menuMusic, bossMusic;
  private static final String PATH = "/home/kai/Arbeitsfläche/repos/mmm-binding-of-bleistift/BOB/data/";
  
  private static final String MENU_MUSIC = "music/menu_long.wav";
  private static final int MENU_MUSIC_LENGTH = 195000;
  private int MENU_MUSIC_LOOP_TIME;
  
  private static final String INGAME_MUSIC = "music/ingameMusic.wav";
  private static final int INGAME_MUSIC_LENGTH = 239000;
  private int INGAME_MUSIC_LOOP_TIME;
  
  private static final String BOSS_MUSIC = "music/bossMusic.wav";

  private static final String SHOOT_SOUND = "sfx/playerShot.wav";
  private String[] BUBBLE_SOUND = new String[4];
  private static final String OUT_OF_AMMO_SOUND = "sfx/outofammo.wav";
  private static final String PICKUP_SOUND = "sfx/pickup.wav";
  private static final String HURT_SOUND = "sfx/playerHurt.wav";
  private static final String GAME_OVER_SOUND = "sfx/gameOver.wav";
  private static final String PLANT_BOMB_SOUND = "sfx/plantBomb.wav";
  private static final String BOMB_EXPLOSION_SOUND = "sfx/bombExplosion.wav";
  private static final String LVL_COMPLETED_SOUND = "sfx/lvlCompleted.wav";

  public SoundManager() {
    BUBBLE_SOUND[0] ="sfx/blubberblase0.wav";
    BUBBLE_SOUND[1] ="sfx/blubberblase1.wav";
    BUBBLE_SOUND[2] ="sfx/blubberblase2.wav";
    BUBBLE_SOUND[3] ="sfx/blubberblase3.wav";
  }
  
  public void stopMusic() {
    stopMenuMusic();
    stopIngameMusic();
    stopBossMusic();
  }
  
  public void update() {
    if(menuMusic!=null && MENU_MUSIC_LOOP_TIME < millis()) {
      stopMenuMusic();
      playMenuMusic();
    }
    if(ingameMusic!=null && INGAME_MUSIC_LOOP_TIME < millis()) {
      stopIngameMusic();
      playIngameMusic();
    }
  }

  public void playMenuMusic(){
    if(menuMusic==null) {
      menuMusic = play(MENU_MUSIC);
      MENU_MUSIC_LOOP_TIME = millis() + MENU_MUSIC_LENGTH;
    }
  }
  public void stopMenuMusic(){
    if(menuMusic!=null) menuMusic.destroy();
    menuMusic = null;
  }

  public void playIngameMusic(){
    if(ingameMusic==null) {
      ingameMusic = play(INGAME_MUSIC);
      INGAME_MUSIC_LOOP_TIME = millis() + INGAME_MUSIC_LENGTH;
    }
  }

  public void stopIngameMusic(){
    if(ingameMusic!=null) ingameMusic.destroy();
    ingameMusic = null;
  }
  
  public void playBossMusic(){
    if(bossMusic == null){
       bossMusic = play(BOSS_MUSIC); 
    }
  }
  
  public void stopBossMusic(){
     if(bossMusic != null) bossMusic.destroy();
     bossMusic = null; 
  }

  public void playShootSound(){
    play(SHOOT_SOUND);
  }

  public void playBubbleSound(){ 
    int rnd = (int)random(0,4);
    play(BUBBLE_SOUND[rnd]);
  }

  public void playOutOfAmmoSound(){
    play(OUT_OF_AMMO_SOUND);
  }

  public void playPickupSound(){
    play(PICKUP_SOUND);
  }

  public void playHurtSound(){
    play(HURT_SOUND);
  }

  public void playGameOverSound(){
    play(GAME_OVER_SOUND);
  }
  public void playPlantBombSound(){
    play(PLANT_BOMB_SOUND);
  }

  public void playBombExplosionSound(){
    play(BOMB_EXPLOSION_SOUND);
  }

  public void playLvlCompletedSound(){
    play(LVL_COMPLETED_SOUND);
  }
  private Process play(String file) {
    try {
      return Runtime.getRuntime().exec("aplay "+PATH + file);
    } catch (Exception e) {
      return null;
    }
  }
}
