interface Movable {
  public void updatePosition();
  public void collision(WObject o);
}

