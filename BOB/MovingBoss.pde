class MovingBoss extends BossEnemy {
  
  public static final int MOVING_BOSS_HP = BOSS_ENEMY_HP;
  public static final int MOVING_BOSS_SPEED = 2;
  public static final int MOVING_BOSS_DMG = 5;
  
  public int getDmgGain() { return 3; }
  public int getHpGain()  { return BOSS_ENEMY_HP / 2; }

  int dir = 2;
  int attackDir = 0;

  private int startMoveTime = 0; 
  private int movingCD = 600;     // delay till direction change

  public MovingBoss(int x, int y) {
    super(x, y, 32, 32, BOSS_ENEMY_HP, 5, 2);
    if(currLvl != null) this. setMaxHp(MOVING_BOSS_HP+(calculateLevelFactor()*5));
    this.stages = 3;
    this.cooldown = 200;
  } 

  public int getAttackDir() {
    return NOTHING;
  }
  
  public void possibleAttack() {

    if (this.stages == 3 && (this.hp <= (int)(BOSS_ENEMY_HP*0.75))) {
      this.stages--;
      currRoom.spawn("Fly", 3, this.x, this.y);
    }
    if (this.stages == 2 && (this.hp <= (int)(BOSS_ENEMY_HP*0.5))) {
      this.stages--;
      this.speed++;
      currRoom.spawn("Fly", 3, this.x, this.y);
    }
    if (this.stages == 1 && (this.hp <= (int)(BOSS_ENEMY_HP*0.25))) {
      this.stages--;
      this.speed++;
      currRoom.spawn("Fly", 4, this.x, this.y);
    }
    
    super.possibleAttack();
    // in case we want to add attacks (in getAttackDir != NOTHING),
    // they wouldn't trigger if we forgot to do this.
  }

  public int movementAI(){  
    
    if(startMoveTime + movingCD < millis()){
      startMoveTime = millis();
      dir = (int)random(0,4);
    }
    return dir;
  }

  public void hurt(int dmg) {
    this.hp -= dmg;
    if (this.hp <= 0) {
      die();
    }
  }

  public void die() {
    // possible other stuff, eg drop items
    this.markForDespawn(true);
  }

  public void drawIt() {
    this.animationCounter = graphicsManager.showMovingBossImage(this.getX(), this.getY(), this.calcSpeed(), this.getMoveDir(), this.animationCounter);
    showHp(SHOW_BOSS_ENEMY_HP, MOVING_BOSS_HP);
  }
  public void reactToObstacleCollision(int dir_from_where_collided){
    dir = dir_from_where_collided;
  }
  
}

