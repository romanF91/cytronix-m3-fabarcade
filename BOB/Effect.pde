private abstract class Effect {
  
int frame = 0;
boolean over = false;
boolean stopgame;

public Effect (boolean stopgame) {
  this.stopgame = stopgame;
}

public abstract void drawIt();

}
