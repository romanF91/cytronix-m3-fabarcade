class BasicAttack extends Weapon {
  
  public BasicAttack(int x, int y, int width, int height, int level, boolean b) {
    super(x, y, width, height, level, b);
    this.setName("BASIC ATTACK");
    this.INIT_CHARGES = 0; // infinite anyways
    this.INIT_COOLDOWN = 400;
    this.CD_BUFF_PER_LEVEL = 0;
    this.CHARGES_BUFF_PER_LEVEL = 0;
    this.attacksPlayer = b;
  }
  
  public void activate(){
    // nothing
  }
  
  public void shoot(int startX, int startY, int dir) {
    this.spawnProjectiles(startX, startY, dir);
    soundManager.playShootSound();
  }
  
  protected void spawnProjectiles(int startX, int startY, int dir){
    int SPACE_BETWEEN = 5;
    if (dir == UP || dir == DOWN) {
      currRoom.getObjects().add(new PrototypeProjectile(startX, startY, dir, this.attacksPlayer));
    } else if (dir == LEFT || dir == RIGHT) {
      currRoom.getObjects().add(new PrototypeProjectile(startX, startY, dir, this.attacksPlayer));
    } else {
      throw new Error("WEAPON ERROR ?!");
    }
  }
  
  public void drawIt() {
    fill(148, 0, 211);
    rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
  }
  
  protected int getInitCharges(){
    return INIT_CHARGES;
  }
  
}
