class Level {
  //private Room rStart;
  private Room [][] rooms;
  private int roomCounter = 0;
  private int sRoomCounter;
  private int type;
  private int number;
  final int MAP_WIDTH = 5;
  final int MAP_HEIGHT = 5;

  public Level(Room startRoom, int counter, int type, int number) {
    this.number = number;
    this.roomCounter = counter;
    this.type = type; 
    this.rooms = new Room[MAP_HEIGHT][MAP_WIDTH];
    this.sRoomCounter = (int)(MAP_WIDTH/2);
    this.rooms[sRoomCounter][sRoomCounter] = startRoom;
    roomSpawn(rooms[sRoomCounter][sRoomCounter], false);
  }

  public void setStartRoom(Room r) {
    this.rooms[sRoomCounter][sRoomCounter] = r;
  }

  public Room getStartRoom() {
    return this.rooms[sRoomCounter][sRoomCounter];
  }
  
  public void setLevelType(int type) {
    this.type = type;
  }

  public int getLevelType() {
    return this.type;
  }

  public void setRoomCounter(int c) {
    this.roomCounter = c;
  }

  public int getRoomCounter() {
    return this.roomCounter;
  }
  
  public Room [][] getRooms(){
     return this.rooms; 
  }
  
  public int getNumber(){
     return this.number; 
  }
  
  public void setNumber(int n){
     this.number = n; 
  }
  
  
  // The following method checks if all rooms EXCEPT THE BOSS ROOM in the level are cleared and returns a boolean
  public boolean isCompleted(){
     Room[][] rooms = this.getRooms();
     for(int i = 0; i<5; i++){
        for(int j = 0; j < 5; j++){
            if (rooms[i][j] == null) continue;
            if (rooms[i][j].isBossRoom()) continue;
            if (!rooms[i][j].isCompleted()) return false; //return false if there is any uncleared room
        } 
     }
     return true; // return true if there are no uncleared rooms
  }

  /*
  / The following Methods recursively add Rooms to a given Room
   / \param room Room you want to add to
   / \param dir Direction you want to add the Room
   / \param b boolean to mark boss room
   */

  private void addRoomHelper(int y, int x, int dir, boolean b) {
    int newX = x;
    int newY = y;
    int oldDir = 0;
    
    switch(dir){
     case UP :
      newY--; 
      oldDir = DOWN;
      break;
     case DOWN :
      newY++;
      oldDir = UP;
      break;
     case LEFT :
      newX--;
      oldDir = RIGHT;
      break;
     case RIGHT :
      newX++;
      oldDir = LEFT;
      break;
    }
    
    // Error cases
    if(newX < 0){
      addRoomHelper(y, x, UP, b);
      return;
    }
    if(newX >= MAP_WIDTH){
      addRoomHelper(y, x, DOWN, b);
      return;
    }
    if(newY < 0){
      addRoomHelper(y, x, RIGHT, b);
      return;
    }
    if(newY >= MAP_HEIGHT) {
      addRoomHelper(x, y, LEFT, b);
      return;
    }
    if(this.rooms[newY][newX] != null){
      addRoomHelper(newY, newX, (int)random(0,4), b);
      return;
    }
    
    //add new room
    this.rooms[newY][newX] = new Room(b);
    roomSpawn(this.rooms[newY][newX], false);
    
  }

  // adds Rooms to Level
  public void addRooms() {
    boolean isBoss = false;
    for (int i = 0; i < this.getRoomCounter()-1; i++) { 
      if (i == this.getRoomCounter()-2) isBoss = true;
      addRoomHelper(this.sRoomCounter, this.sRoomCounter, (int) random(0, 4), isBoss);
    }
    addDoors();
  }
  
  public void addDoors(){
     for(int i = 0; i < MAP_HEIGHT; i++){
        for(int j = 0; j < MAP_WIDTH;j++){
           if(rooms[i][j] == null) continue;
           boolean bossRoom = rooms[i][j].isBossRoom();
           if( i-1 >= 0 && rooms[i-1][j] != null){
              if(rooms[i-1][j].isBossRoom()) rooms[i][j].addDoor(UP, true);
              else rooms[i][j].addDoor(UP, bossRoom);
           }
           if( j-1 >= 0 && rooms[i][j-1] != null){
              if(rooms[i][j-1].isBossRoom()) rooms[i][j].addDoor(LEFT, true);
              else rooms[i][j].addDoor(LEFT, bossRoom);
           }
           if(i+1 < MAP_HEIGHT && rooms[i+1][j] != null){
              if(rooms[i+1][j].isBossRoom()) rooms[i][j].addDoor(DOWN, true);
              else rooms[i][j].addDoor(DOWN, bossRoom);
           }
           if(j+1 < MAP_WIDTH && rooms[i][j+1] != null){
              if(rooms[i][j+1].isBossRoom()) rooms[i][j].addDoor(RIGHT, true);
              else rooms[i][j].addDoor(RIGHT, bossRoom);
           }
        }
     } 
  }

  // adds player to room if there is no
  public void addPlayer(Room r) {
    int counter = 0;
    for (WObject o : r.getObjects()) {
      if (o instanceof Player) counter++;
    } 

    if (counter == 0) {
      r.getObjects().add(player);
    }
  }

  // The following method is used to spawn enemies in a level
  // The number and kind of enemies differs for every leveltype
  public boolean roomSpawn(Room r, boolean bossRoom) {

    boolean success=false;
    if (this.type == 1) {
      if (!r.isBossRoom()) {
        success = r.spawn("AntiFly", (int)random(1,3));
        success = r.spawn("Stone", (int)random(15, 21));
        success = r.spawn("Fly", (int)random(1, 4));
        success = r.spawn("MovingEnemy", (int)random(1, 2));
        if(currLvl != null && currLvl.getNumber() > 1) 
         success = r.spawn("CirclingEnemy", (int)random(1,4));
      }
      else {
        success = r.spawn("MovingBoss", 1);
      }   
    }
    if (this.type == 2) {
      if (!r.isBossRoom()) {
        success = r.spawn("Stone", (int)random(15, 21));
        success = r.spawn("Fly", (int)random(1, 3));
        success = r.spawn("MovingEnemy", (int)random(0, 2));
        success = r.spawn("SideEnemy", (int)random(1, 3));
        if(currLvl != null && currLvl.getNumber() > 4)
         success = r.spawn("CirclingEnemy", (int)random(1,4));
      }
      else {
        success = r.spawn("BombingFlyBoss", 1);
      }   
    }
    if (this.type == 3) {
      if (!r.isBossRoom()) {
        success = r.spawn("Stone", (int)random(15, 21));
        success = r.spawn("CirclingEnemy", (int)random(2,4));
        success = r.spawn("Fly", (int)random(1, 4));
        success = r.spawn("SideEnemy", (int)random(0, 3));
        if(currLvl != null && currLvl.getNumber() > 8) 
         success = r.spawn("MovingEnemy", (int)random(1,4));
      }
      else {
        if(((int)random(0,2)) == 0)
          success = r.spawn("MovingBoss", 1);
        else
          success = r.spawn("BombingFlyBoss", 1);
      }   
    }
    if (this.type == 4) {
      if (!r.isBossRoom()) {
        success = r.spawn("Stone", (int)random(15, 21));
        success = r.spawn("CirclingEnemy", (int)random(3,5));
        success = r.spawn("MovingEnemy", (int)random(0, 3));
        success = r.spawn("SideEnemy", (int)random(1, 3));
        if(currLvl != null && currLvl.getNumber() > 4) 
         success = r.spawn("AntiFly", (int)random(1,4));
      }
      else {
        success = r.spawn("SplittingBoss", 1);
      }   
    }
    if (this.type == 5) {
      if (!r.isBossRoom()) {
        success = r.spawn("Stone", (int)random(15, 21));
        success = r.spawn("MovingEnemy", (int)random(0, 2));
        success = r.spawn("SideEnemy", (int)random(1, 3));
        success = r.spawn("Fly", (int)random(1, 4));
        if(currLvl != null && currLvl.getNumber() > 6) 
         success = r.spawn("CirclingEnemy", (int)random(1,4));
      }
      else {
        int random = (int)random(0,3);
        if(random == 0)
          success = r.spawn("MovingBoss", 1);
        else if(random == 1)
          success = r.spawn("BombingFlyBoss", 1);
        else if(random == 2)
          success = r.spawn("SplittingBoss", 1);
      }   
    }
    
     // r.completed = false;
     // the line above seems to be not necessary, lets try it without

    return success;
  }

  // loads the next room
  // updates object list and playerposition
  public void loadNextRoom(int r) {
    int newX = 0;
    int newY = 0;
    int playerX = 0;
    int playerY = 0;
    
    for(int i = 0; i < MAP_HEIGHT; i++){
       for(int j = 0; j < MAP_WIDTH; j++){
         if(this.rooms[i][j] != null){
          if(this.rooms[i][j].isCurrRoom()){
             newX = j;
             newY = i;
             break; 
          }
         }
       } 
    }
    
    this.rooms[newY][newX].setCurrRoom(false);
    
    switch(r){
     case UP :
      newY--; 
      playerX = (ROOM_WIDTH/2-player.getWidth()/2);
      playerY = (ROOM_HEIGHT + HUD_HEIGHT -18 - player.getHeight());
      break;
     case DOWN :
      newY++;
      playerX = (ROOM_WIDTH/2-player.getWidth()/2);
      playerY = (HUD_HEIGHT+18);
      break;
     case LEFT :
      newX--;
      playerX = ROOM_WIDTH-18 - player.getWidth();
      playerY = ROOM_HEIGHT/2 + HUD_HEIGHT - player.getHeight()/2;
      break;
     case RIGHT :
      newX++;
      playerX = 18;
      playerY = ROOM_HEIGHT/2 + HUD_HEIGHT - player.getHeight()/2;
      break;
    }
    
    currRoom = this.rooms[newY][newX];
    currRoom.setCurrRoom(true);
    addPlayer(currRoom);
    player.setX(playerX);
    player.setY(playerY); 
  } 
}

