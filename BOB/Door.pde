class Door extends StaticObject {
  private boolean open;
  private boolean isBossDoor;
  private int position;
  
  public Door(int x, int y, int position, boolean status, boolean isBossDoor) {
    super(x, y, 16, 16);
    this.open = status;
    this.position = position;
    this.isBossDoor = isBossDoor;
  }
  
  public void open(){
     if(!this.isBossDoor){
       this.open = true; 
     }else{
        if(currLvl.isCompleted()){
          this.open = true;
        }
     }
  }
  
  public int getPosition(){
     return this.position;
  }
  
  public void close(){
     this.open = false; 
  }
  
  public boolean isOpen(){
     return this.open; 
  }
  
  // draws doors (two colors: white = open, dark = closed)
  public void drawIt(){
    if(this.open){
      
      //if(this.isBossDoor) fill(255, 255, 125);
      //else fill(255);
      //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
      graphicsManager.showDoorOpenImage(this.getX(), this.getY(), this.isBossDoor);
    }else{
      //if(this.isBossDoor) fill(130, 105, 30);
      //else fill(30);
      //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
      graphicsManager.showDoorCloseImage(this.getX(), this.getY(), this.isBossDoor);
    }
  }
}
