class Stone extends Obstacle{
  int animationCounter = 0;
  
  public Stone(int x, int y, int width, int height){
    super(x, y, width, height);
  }
  public void drawIt(){
    //fill(119,136,153);
    //rect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    
    // shows obstacle image; last param selects which type of obstacle is shown (1 = wooden box, ...)
    this.animationCounter = graphicsManager.showBoxAnimation(this.getX(), this.getY(), 15, UP, this.animationCounter, currLvl.getNumber());
  }
}
